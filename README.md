# *willy*, a Linux Markdown text-formatting program producing output suitable for simple fixed-width terminal windows.
***
## LICENSE
**willy** is covered by the GNU General Public License (GPL) version 3 and above.
## MARKDOWN CHEATSHEET
Here are some examples of transforming Markdown directives into text.
### Headers
``` Bash
# Header Level 1
## Header Level 2
### Header Level 3
#### Header Level 4
##### Header Level 5
###### Header Level 6
___
```
![Headers](./README_images/Headers.png  "Headers")
### Emphasis
``` Bash
# Emphasis
Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~
```
![Emphasis](./README_images/Emphasis.png  "Emphasis")
### Lists
``` Bash
# Lists
1. First ordered list item
2. Another item
  * Unordered sub-list. 
1. Actual numbers don't matter, just that it's a number
  1. Ordered sub-list
4. And another item.

   You can have properly indented paragraphs within list items. Notice the blank line above, and the leading spaces (at least one, but we'll use three here to also align the raw Markdown).

   To have a line break without a paragraph, you will need to use two trailing spaces.⋅⋅
   Note that this line is separate, but within the same paragraph.

* Unordered list can use asterisks
- Or minuses
+ Or pluses
___
```
![Lists](./README_images/Lists.png  "Lists")
### Links
``` Bash
# Links
I'm an inline-style link](https://www.google.com)

[I'm an inline-style link with title](https://www.google.com "Google's Homepage")

[I'm a relative reference to a repository file](../blob/master/LICENSE)
___
```
![Links](./README_images/Links.png  "Links")
### Images
``` Bash
# Images
Inline-style: 
![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")
___
```
![Images](./README_images/Images.png  "Images")
### Code
In this paragraph, the three back-ticks are replaced by the keyword "_<code>_".
``` Bash
# Code
<code>javascript
var s = "JavaScript syntax highlighting";
alert(s);
<code>
 
<code>python
s = "Python syntax highlighting"
print s
<code>
 
<code>
No language indicated, Bash assumed!
<code>

    var s = "JavaScript syntax highlighting";
    alert(s);

    s = "Python syntax highlighting"
    print s

    No language indicated, Bash assumed! 
    #include "pager.h"
    int pager_view( char *filename,  char *header, enum epager_view viewtype);
    FILE *fp = NULL; 
___
```
With syntax highlighting:
![Codewsc](./README_images/Codes_wsc.png  "With syntax coloring")
Without syntax highlighting:
![Codewosc](./README_images/Codes_wosc.png  "Without syntax coloring")
Without user's code block configuration { background = white, foreground = black, attribut = normal }
![Codewosc_wb_](./README_images/Codes_wosc_wb.png  "Without syntax coloring")
### Blockquotes
In this paragraph, the blockquoute charactere is replaced by the keyword "_<quote>_".
``` Bash
# Blockquotes
<quote> Blockquotes are very handy in email to emulate reply text.
<quote> This line is part of the same quote.

Quote break.

<quote>  This is a very long line that will still be quoted properly when it wraps. Oh boy let's keep writing to make sure this is long enough to actually wrap for everyone. Oh, you can *put* **Markdown** into a blockquote.
___
```
![Blockquotes](./README_images/Blockquotes.png  "Blockquotes")
### Horizontal Lines
``` Bash
# Horizontal Rules
Three or more...

---
Hyphens

***
Asterisks

___
Underscores
___
```
![HLines](./README_images/HLines.png  "HLines")
### Line Breaks
``` Bash
# Line Breaks
# Line Breaks
Here's a line for us to start with.


This line is separated from the one above by two newlines, so it will be a *separate paragraph*.

This line is also separated, but...
This line is only separated by a single newline, so it's a separate line in the *same paragraph*.


1. Ordered list item.
This line is separated from the one above by 0 newline; should be **concatenated** to it.
2. Ordered list item

This line is separated from the one above by 1 newline; it's a **separate not indented line**.
3. Ordered list item

   This line is separated from the one above by 1 newline; it's a **separate indented line**.
4. Ordered list item


This line is separated from the one above by 2 newlines; it's a **separate paragraph**.
___
```
![LBreaks](./README_images/LBreaks.png  "LBreaks")
### Tables
``` Bash
# Tables
| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

|:------------|--------|
|**Name**    |John Doe|
|**Position**|CEO     |

| Col 1         | Col 2         | Col 3 |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 | Lorem ipsum dolor sit amet |
| zebra stripes | are neat|
| zebra stripes | are neat      |    $1 | voluptua | sed diam |

> | Tables        | Are           | Cool  |
> | ------------- |:-------------:| -----:|
> | col 3 is      | right-aligned | $1600 |
> | col 2 is      | centered      |   $12 |
> | zebra stripes | are neat      |    $1 |
___
```
![Tables](./README_images/Tables.png  "Tables")

The user can choose the type of presentation of the tables via the **willy** configuration file. 4 classes are available:

![Class](./README_images/Classes_of_Tables.png  "Classes of tables")

## DISPLAYING REFERENCED IMAGES
**willy** gives the possibility with options *--show* and *--multishow* to view all the referenced images in the Markdown file. It relies for this on the [feh](https://feh.finalrewind.org/) tool, a fast and light image viewer. *feh* is shipped by many Linux/BSD distributions, including Arch Linux, Debian, FreeBSD, OpenBSD and Ubuntu. It is released under a variant of the MIT license.

Under Ubuntu, install beforehand using the command:
``` Bash
sudo apt install feh
```
Example with *--show* and *--more* options:
![Example](./README_images/FEH.png  "Example with --show and --more options.")

There are 2 modes for showing images: slideshow mode (*--show* option) and multiple windows mode (*--multishow* option). Whatever the mode chosen, **willy** displays the report of *feh* on the referenced images (not supported format (by *feh*), non-existing images, etc.).
## HTML AND TEXT REPORTS
With **willy**, you also can obtain with the *--report* or *--text* options, a HTML or a text report summarizing all the links and images referenced in processed Markdown files. You can so check the accuracy of the links.

Example of HTML report:
![Example](./README_images/HReport.png  "Example of HTML report.")

Example of text report:
![Example](./README_images/TReport.png  "Example of text report.")
## SYNTAX COLORING
By default, syntax highlighting is enable for code block. If the language identifier is not set, then "bash" is assumed.

Syntax highlighting is based on Lorenzo Bettini's *source-highlight* tool (https://www.gnu.org/software/src-highlite). If this tool is not installed (and accessible via the PATH mechanism) or if the *--no-syntax-coloring* option is used, then the standard display is done. Refer to the *source-highlight* web site for the input languages supported.

Note: The syntax highlighting can be disable in the *.willyrc* configuration via: *syntax-highlighting=off*.

## USAGE
``` bash
$ willy --help
willy - Copyright (c) 2018, Michel RIZZO. All Rights Reserved.
willy - Version 1.7.0

Markdown text-formatting program

Usage: willy [OPTIONS]... [FILES]...

  -h, --help                    Print help and exit
      --full-help               Print help, including hidden options, and exit
  -V, --version                 Print version and exit
  -o, --output=FILE             Output file. Stdout by default
  -m, --more                    To view output one page at a time
                                  (default=off)
  -n, --nodisplay               No display  (default=off)
  -j, --justify                 Justification of text  (default=off)
  -s, --show                    Show referenced images (slideshow mode)
                                  (default=off)
  -w, --multishow               Show referenced images (multiple windows mode)
                                  (default=off)
  -t, --text                    Generation of text report (links and images)
                                  (default=off)
  -X, --no-syntax-coloring      No syntax oloring in code sections
                                  (default=off)
  -r, --report=FILE             Generation of HTML report (links and images)
  -c, --columns=NUMBER          Number of columns to consider to display
      --config=CONFIGURATION FILE
                                File where is stored configuration

Exit: returns a non-zero status if an error is detected.

$
```
Example with *--justify* and *--more* options:
![Example](./README_images/More.png "Example with --justify and --more options.")

Example with *--nodisplay* and *--show* options:
![Example](./README_images/Nodisplay.png "Example with --nodisplay and --show options.")

Example of HTML report:
![Example](./README_images/Report.png "Example of HTML report.")
## CONFIGURATION FILE
The coloring and attributes of the lexical entities handled by **willy** can be configured with a *.willyrc* file placed in your home directory. In addition, the option *--config* allows to explicitely specify an additional configuration file. The file *willyrc* is provided as example.
```
; Configuration file example for willy.
; ====================================
;
; General purposes:
; syntax highlighting=on | off
; Default is 'on'
: 
[general]
syntax-highlighting=off

; Markdown items:
; --------------
; For all items:
;   foregroundcolor=black | red | green | yellow | blue | magenta | cyan | white | normal
;   attribut= bold, boldunderline, underline, italic, normal
; Specific to [code]:
;   backgroundcolor=black | red | green | yellow | blue | magenta | cyan | white | normal
[header1]
foregroundcolor=yellow
attribut=bold
[header2]
foregroundcolor=yellow
attribut=boldunderline
[header3]
foregroundcolor=yellow
attribut=bold
[header4]
foregroundcolor=yellow
attribut=underline
[header5]
foregroundcolor=yellow
attribut=italic
[header6]
foregroundcolor=yellow
attribut=normal
[code]
foregroundcolor=black
backgroundcolor=yellow
attribut=normal
;foregroundcolor=green
;attribut=normal
[link]
foregroundcolor=blue
attribut=bold
[image]
foregroundcolor=red
attribut=bold

; Table:
; -----
; class=default | horizon | zebra | reverse
;
[table]
class=reverse
;--------------------------------------------------------------------------------------------
```

Note:
   - *backgroundcolor*, *foregroundcolor* and *attribut* for code block are valid only if option *--no-syntax-coloring* (or *-X*) is set or if the syntax highlighting tool is not installed.

## STRUCTURE OF THE APPLICATION
This section walks you through **willy**'s structure. Once you understand this structure, you will easily find your way around in **willy**'s code base.

``` bash
$ yaTree
./                                # Application level
├── README_images/                # Images for documentation
│   ├── Blockquotes.png           # 
│   ├── Classes_of_Tables.png     # 
│   ├── Codes_wosc.png            # 
│   ├── Codes_wosc_wb.png         # 
│   ├── Codes_wsc.png             # 
│   ├── Emphasis.png              # 
│   ├── FEH.png                   # 
│   ├── HLines.png                # 
│   ├── HReport.png               # 
│   ├── Headers.png               # 
│   ├── Images.png                # 
│   ├── LBreaks.png               # 
│   ├── Links.png                 # 
│   ├── Lists.png                 # 
│   ├── More.png                  # 
│   ├── Nodisplay.png             # 
│   ├── TReport.png               # 
│   └── Tables.png                # 
├── src/                          # Source directory
│   ├── inih/                     # inih (INI Not Invented Here) file parser
│   │   ├── LICENSE.txt           # License text file
│   │   ├── Makefile              # Makefile
│   │   ├── README.md             # ReadMe Mark-Down file
│   │   ├── ini.c                 # Source file
│   │   └── ini.h                 # Header file
│   ├── willy/                    # Markdown text-formatting program
│   │   ├── Makefile              # Makefile
│   │   ├── willy.c               # Main source file
│   │   ├── willy.ggo             # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
│   │   ├── willy_code.c          # Text/Code support
│   │   ├── willy_code.h          # Text/Code support header file
│   │   ├── willy_common.h        # Common header file
│   │   ├── willy_configuration.c # Configuration source file
│   │   ├── willy_configuration.h # Configuration header file
│   │   ├── willy_drawingchars.h  # Drawing characters definition
│   │   ├── willy_escape.c        # scape code sequence definition source file
│   │   ├── willy_escape.h        # Escape code sequence definition header file
│   │   ├── willy_feh.c           # FEH management source file
│   │   ├── willy_feh.h           # FEH management header file
│   │   ├── willy_funcs.c         # Temporary files and memory blocks management source file
│   │   ├── willy_funcs.h         # Temporary files and memory blocks management header file
│   │   ├── willy_markdown.c      # Markdown decoding source file
│   │   ├── willy_markdown.h      # Markdown decoding header file
│   │   ├── willy_report.c        # Links and images generator source file
│   │   ├── willy_report.h        # Links and images generator header file
│   │   ├── willy_table.c         # Text/Table support
│   │   ├── willy_table.h         # Text/Table support header file
│   │   ├── willy_text.c          # Text coding source file
│   │   └── willy_text.h          # Text coding header file
│   └── Makefile                  # 
├── test/                         # Test directory
│   ├── 1.jpeg                    # Picture referenced in test files
│   ├── 2.jpeg                    # Picture referenced in test files
│   ├── file1.md                  # Test file #1 referenced in README.md file
│   └── file2.md                  # Test file #2
├── COPYING.md                    # GNU General Public License markdown file
├── LICENSE.md                    # License markdown file
├── Makefile                      # Makefile
├── README.md                     # ReadMe Markdown file
├── RELEASENOTES.md               # Release Notes Mark-Down file
├── VERSION                       # Version identification text file
└── willyrc                       # Example of configuraton file

5 directories, 58 files
$
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd willy
$ make clean all
```
## HOW TO INSTALL AND USE THIS APPLICATION
```Shell
$ cd willy
$ make release
    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    # We consider that $BIN_DIR is a part of the PATH.
```
## LIMITATIONS
Will not be supported:
- Inline HTML.
- Alternatives for H1 and H2, with an underline-ish style.
- Reference-style link:
``` Bash
[I'm a reference-style link][Arbitrary case-insensitive reference text]
arbitrary case-insensitive reference text]: https://www.mozilla.org
```
- Use of number for reference style link definition:
``` Bash
[You can use numbers for reference-style link definitions][1]
[1]: http://slashdot.org
```
- Reference-style link to image:
``` Bash
Reference-style: [alt text][logo] 
[logo]: https://github.com/adam-p/Markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 2" 
```
- Direct YouTube Videos.

## NOTES
- In the syntax highlighting mode, lines of code blocks longer than the width of the terminal (with the decoration) are truncated. If syntax highlight mode is disable, then they are collapsed.
- Application works fine with *xterm*, *uxtem*, *terminator* and *tilix*.
- Application developed and tested with gcc 7.3.0 running on XUBUNTU 18.04.

## SOFTWARE REQUIREMENTS
- For usage:
    - *source-highlight* GNU Source-highlight 3.1.8 (library: 4:1:0). Refer to https://www.gnu.org/software/src-highlite.
- For development:
    - *GengetOpt* binary package installed, version 2.22.6.
    - *libPager* library. Refer to https://gitlab.com/mubunt/libPager.

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***