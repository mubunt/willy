___
# Header Level 1
## Header Level 2
### Header Level 3
#### Header Level 4
##### Header Level 5
###### Header Level 6
___
# Emphasis
Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~
___
# Lists
1. First ordered list item
2. Another item
  * Unordered sub-list. 
1. Actual numbers don't matter, just that it's a number
  1. Ordered sub-list
4. And another item.

   You can have properly indented paragraphs within list items. Notice the blank line above, and the leading spaces (at least one, but we'll use three here to also align the raw Markdown).

   To have a line break without a paragraph, you will need to use two trailing spaces.
   Note that this line is separate, but within the same paragraph.

* Unordered list can use asterisks
- Or minuses
+ Or pluses
___
# Links
[I'm an inline-style link](https://www.google.com)

[I'm a relative reference to a repository file](../../willy/LICENSE)
___
# Images
Inline-style: 
![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")
___
# Code
```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```
 
```python
s = "Python syntax highlighting"
print s
```
 
```
No language indicated, Bash assumed!
```
    var s = "JavaScript syntax highlighting";
    alert(s);

    s = "Python syntax highlighting"
    print s

    No language indicated, Bash assumed! 
    #include "pager.h"
    int pager_view( char *filename,  char *header, enum epager_view viewtype);
    FILE *fp = NULL;
___
# Blockquotes
> Blockquotes are very handy in email to emulate reply text.
> This line is part of the same quote.

Quote break.


> This is a very long line that will still be quoted properly when it wraps. Oh boy let's keep writing to make sure this is long enough to actually wrap for everyone. Oh, you can *put* **Markdown** into a blockquote. 
___
# Horizontal Rules
Three or more...

---
Hyphens

***
Asterisks

___
Underscores
___
# Line Breaks
Here's a line for us to start with.


This line is separated from the one above by two newlines, so it will be a *separate paragraph*.

This line is also separated, but...
This line is only separated by a single newline, so it's a separate line in the *same paragraph*.


1. Ordered list item.
This line is separated from the one above by 0 newline; should be **concatenated** to it.
2. Ordered list item

This line is separated from the one above by 1 newline; it's a **separate not indented line**.
3. Ordered list item

   This line is separated from the one above by 1 newline; it's a **separate indented line**.
4. Ordered list item


This line is separated from the one above by 2 newlines; it's a **separate paragraph**.
# Tables
| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

|:------------|--------|
|**Name**    |John Doe|
|**Position**|CEO     |

| Col 1         | Col 2         | Col 3 |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 | Lorem ipsum dolor sit amet |
| zebra stripes | are neat|
| zebra stripes | are neat      |    $1 | voluptua | sed diam |

> | Tables        | Are           | Cool  |
> | ------------- |:-------------:| -----:|
> | col 3 is      | right-aligned | $1600 |
> | col 2 is      | centered      |   $12 |
> | zebra stripes | are neat      |    $1 |
___
