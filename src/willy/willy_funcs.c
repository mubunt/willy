//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: willy
// Linux Markdown text-formatting program producing output suitable for simple
// fixed-width terminal windows.
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdbool.h>
#include <linux/limits.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "willy_common.h"
#include "willy_funcs.h"
//------------------------------------------------------------------------------
// EXTERNAL VARIABLES
//------------------------------------------------------------------------------
extern int		_temporary_files;		// Number of temporary files still to be deleted
extern int		_allocated_memory;		// Number of allocated memories still to be deleted
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static const char 	*tmptemplate[] = {
	"/tmp/willyTemporaryXXXXXX",
	"/tmp/willyFilelistXXXXXX",
	"/tmp/willycoloringXXXXXX",
	"/tmp/willyLogfileXXXXXX"
};
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS - TEMPORARY FILES MANAGEMENT
//------------------------------------------------------------------------------
char *file_gettemporaryfilename( char *name, tmptype type ) {
	int fd;
	strcpy(name, tmptemplate[type]);
	if (-1 == (fd = mkstemp(name))) {
		error("%s", "Cannot create temporary file.");
		return NULL;
	}
	close(fd);
	++_temporary_files;
	return name;
}
//------------------------------------------------------------------------------
int file_opentemporaryfile( char *name, tmptype type ) {
	int fd;
	strcpy(name, tmptemplate[type]);
	if (-1 == (fd = mkstemp(name)))
		error("%s", "Cannot create temporary file.");
	else
		++_temporary_files;
	return fd;
}
//------------------------------------------------------------------------------
void file_unlinktemporaryfile( char *name ) {
	unlink(name);
	--_temporary_files;
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS - MEMORY BLOCKS MANAGEMENT
//------------------------------------------------------------------------------
void myfree( void *p ) {
	if (p != NULL) {
		free(p);
		--_allocated_memory;
	}
}
//-----------------------------------------------------------------------------
char *mystrdup( const char *s ) {
	++_allocated_memory;
	return strdup(s);
}
//-----------------------------------------------------------------------------
void *mycalloc( size_t size, size_t number) {
	void *p;
	if (NULL == (p = malloc( size * number))) {
		error("%s", "No more memory space avalaible for allocation. Abort.");
		myexit(EXIT_FAILURE);
	}
	++_allocated_memory;
	return p;
}
//-----------------------------------------------------------------------------
char *myalloc( size_t number) {
	char *p;
	if (NULL == (p = malloc(number * sizeof(char)))) {
		error("%s", "No more memory space avalaible for allocation. Abort.");
		myexit(EXIT_FAILURE);
	}
	*p = '\0';
	++_allocated_memory;
	return p;
}
//-----------------------------------------------------------------------------
