//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: willy
// Linux Markdown text-formatting program producing output suitable for simple
// fixed-width terminal windows.
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>
#include <stdbool.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "willy_common.h"
#include "willy_text.h"
#include "willy_markdown.h"
#include "willy_configuration.h"
#include "willy_funcs.h"
#include "willy_feh.h"
#include "willy_drawingchars.h"
#include "willy_escape.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define MAXDEPTH_LIST		16
#define MAXBLOCKQUOTES		20
#define SAFETY_FACTOR		5
//------------------------------------------------------------------------------
// TYPEDEFS
//------------------------------------------------------------------------------
typedef enum { NO_SPACE_COMPLEMENT, WITH_SPACE_COMPLEMENT } line_complement;
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
typedef struct {
	size_t linelength;
	char *margleft;
	char *color;
	char *margright;
	alignment alignement;
	line_complement complement;
} printLine_parameters;
//------------------------------------------------------------------------------
// EXTERNAL VARIABLES
//------------------------------------------------------------------------------
extern size_t 				nbCOLS;					// Number of columns of the terminal
extern long int 			startLastListText;		// Index of the last text of a bullet of a list
extern lexical 				previousMarkdown;		// Previous lexical item processes
extern alignment 			text_alignment;			// Alignment requested by user
extern bool 				syntax_coloring;		// Indicator if syntax coloring available
extern unsigned int 		display_configuration;	// Configuration as defined in configuration file
extern ansiescapesequence	escapeSequence;			// ANSI Escape sequence for test attributs and colors
//------------------------------------------------------------------------------
// EXPORTABLE VARIABLES
//------------------------------------------------------------------------------
char 						*marginleftblockquote 			=	NULL;
char 						*currentAdditionalLeftMargin 	=	NULL;
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static int 					idxDepthList					=	-1;
static int 					tabDepthList[MAXDEPTH_LIST];
static int 					tabIndexOrdList[MAXDEPTH_LIST];
static char 				*marginleft 					=	NULL;
static char 				*marginright 					=	NULL;
static char 				*marginleftfirstline 			=	NULL;
static const int 			arabics[] 						=	{
	1,    4,   5,   9,    10,  40,   49,   50,   90,
	99,   100, 400,  499,  500, 900,  999,  1000
};
static const char 			*romans[] 						=	{
	"i", "iv", "v", "ix", "x", "xl", "il", "l", "xc",
	"ic", "c", "cd", "id", "d", "cm", "im", "m"
};
static bool					strong_attribut 				=	false;	// 'strong_attribut' attribut on going between 2 lines
static bool					emphasis_attribut 				=	false;	// 'emphasis_attribut' attribut on going between 2 lines
static bool					strike_attribut 				=	false;	// 'strike_attribut' attribut on going between 2 lines
static bool					link_attribut 		 			= 	false;	// 'link_attribut' attribut on going between 2 lines
static bool					image_attribut 		 			= 	false;	// 'image_attribut' attribut on going between 2 lines
//------------------------------------------------------------------------------
// MACROS CODE DEFINITIONS
//------------------------------------------------------------------------------
#define GET_INFO(str, deb, end)			{ size_t size = (size_t)(end - deb); strncpy(str, deb + 1, size); str[size - 1] = '\0'; }
//#define MOVE_INFO(dest, pt)				{ strncpy(dest, pt, strlen(pt)); dest += strlen(pt); }
#define MOVE_INFO(dest, pt)				{ strcpy(dest, pt); dest += strlen(pt); }
#define STORE_INFO(dest, c)				{ *dest = c; ++dest; }
#define COPY_CHARACTER(dest, str)		{ *dest = *str; ++dest; }
#define COPY_STRING(dest, str, pte)		{ size_t _k = (size_t)(pte - str + 1); strncpy(dest, str, _k); dest += _k; }

#define PRINT_ATTRIBUT(_ATT)			fprintf(stdout, "%s", _ATT)
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void _replace_enter_with_space( char *str ) {
	while (*str != '\0') {
		if (*str == '\n') *str = ' ';
		++str;
	}
}
//------------------------------------------------------------------------------
//-- Replace image definitions
//-- dest -> convert image definition from str to dest
//--
//-- Input:
//--	![Image 1](/home/X/Y/1.jpeg  "Title: This is Image 1")
//--	![Image 1](/home/X/Y/1.jpeg)
//-- Output:
//--	![This is Image 1]! [/home/X/Y/1.jpeg]
//--	![Image 1]! [/home/X/Y/1.jpeg]
//--
//-- Hypothesis: (to avoid 'malloc / free')
//--	url < 256 characters
//--	title < 256 character
//--	link label < 256 characters
static size_t _replace_images( char *str, char *dest ) {
	size_t count = 0;
	bool backslash = false;
	char urlNtitle[512], url[256], title[256], link[256];
	*dest = '\0';
	while (*str != '\0') {
		if (backslash) {
			backslash = false;
			COPY_CHARACTER(dest, str);
			++str;
			continue;
		}
		if (*str == '\\') {
			backslash = true;
			COPY_CHARACTER(dest, str);
			++str;
			continue;
		}
		if (strncmp(str, MD_IMAGE_START, strlen(MD_IMAGE_START)) != 0) {
			COPY_CHARACTER(dest, str);
			++str;
			continue;
		}
		++str;
		char *pte = str;
		while (*pte != '\0' && *pte != MD_LINK_END) ++pte;
		if (*pte != MD_LINK_END) {
			COPY_STRING(dest, str, pte);
			str = pte;
			continue;
		}
		GET_INFO(link, str, pte);
		++pte;
		if (*pte != MD_URL_START) {
			COPY_STRING(dest, str, pte);
			str = pte;
			continue;
		}
		char *url_part = pte;
		++pte;
		while (*pte != '\0' && *pte != MD_URL_END) ++pte;
		if (*pte != MD_URL_END) {
			COPY_STRING(dest, str, pte);
			str = pte;
			continue;
		}
		++count;
		str = pte + 1;
		GET_INFO(urlNtitle, url_part, pte);
		*title = *url = '\0';
		sscanf(urlNtitle, "%s \"%[^\"]", url, title);
		MOVE_INFO(dest, TXT_IMAGE_START);
		if (*title == '\0' )
			MOVE_INFO(dest, link)
			else
				MOVE_INFO(dest, title);
		MOVE_INFO(dest, TXT_IMAGE_END);
		if (*url != '\0') {
			STORE_INFO(dest, TXT_SEPARATOR);
			STORE_INFO(dest, TXT_URL_START);
			MOVE_INFO(dest, url);
			feh_addFileToFilelist(url);
			STORE_INFO(dest, TXT_URL_END);
		}
	}
	*dest = '\0';
	return count;
}
//------------------------------------------------------------------------------
static void __backslash_underscore( char *from, char *to ) {
	while (*from != '\0') {
		if (*from == '_') {
			*to = '\\';
			++to;
		}
		*to = *from;
		++to;
		++from;
	}
	*to = *from;
}
//-- Replace link definitions
//-- to -> convert link candidates from 'from' to 'to'
//--
//-- Input:
//--	[This is a link](/home/X/Y/1.jpeg)
//--	[This is a link](/home/X/Y/1.jpeg "I am a link")
//--	http://www.example.com
//-- Output:
//--	!%This is a link% [/home/X/Y/1.jpeg]
//--	!%I am a link% [/home/X/Y/1.jpeg]
//--	!%http://www.example.comk% [http://www.example.com]
//--
//-- Hypothesis: (to avoid 'malloc / free')
//--	url < 256 characters
//--	title < 256 character
//--	link label < 256 characters
static size_t _replace_links( char *line, char *to ) {
	char *from = line;
	size_t count = 0;
	bool backslash = false;
	char urlNtitle[512], url[256], burl[256], title[256], link[256];
	*to = '\0';
	while (*from != '\0') {
		if (backslash) {
			backslash = false;
			COPY_CHARACTER(to, from);
			++from;
			continue;
		}
		if (*from == '\\') {
			backslash = true;
			COPY_CHARACTER(to, from);
			++from;
			continue;
		}
		if (*from == MD_LINK_START) {
			//--	[This is a link](/home/X/Y/1.jpeg)
			//--	[This is a link](/home/X/Y/1.jpeg "I am a link")
			if (from != line && (strncmp((from - 1), TXT_IMAGE_START, 2) == 0)) {
				COPY_CHARACTER(to, from);
				++from;
				continue;
			}
			char *endblock = from;
			++endblock;
			int brackets = 0;
			while (*endblock != '\0') {
				if (*endblock == MD_LINK_END && brackets == 0) break;
				if (*endblock == MD_LINK_START) ++brackets;
				if (*endblock == MD_LINK_END) --brackets;
				++endblock;
			}
			if (*endblock != MD_LINK_END) {
				COPY_STRING(to, from, endblock);
				from = endblock;
				continue;
			}
			if (from != line && (strncmp((from - 1), TXT_IMAGE_END, 2) == 0)) {
				COPY_STRING(to, from, endblock);
				from = endblock;
				continue;
			}
			GET_INFO(link, from, endblock);
			++endblock;
			if (*endblock != MD_URL_START) {
				COPY_STRING(to, from, endblock);
				from = endblock;
				continue;
			}
			char *url_part = endblock;
			++endblock;
			while (*endblock != '\0' && *endblock != MD_URL_END) ++endblock;
			if (*endblock != MD_URL_END) {
				COPY_STRING(to, from, endblock);
				from = endblock;
				continue;
			}
			++count;
			from = endblock + 1;
			GET_INFO(urlNtitle, url_part, endblock);
			*title = *url = '\0';
			sscanf(urlNtitle, "%s \"%[^\"]", url, title);
			MOVE_INFO(to, TXT_LINK_START);
			if (*title == '\0' )
				MOVE_INFO(to, link)
				else
					MOVE_INFO(to, title);
			MOVE_INFO(to, TXT_LINK_END);
			if (*url != '\0') {
				STORE_INFO(to, TXT_SEPARATOR);
				STORE_INFO(to, TXT_URL_START);
				__backslash_underscore(url, burl);
				MOVE_INFO(to, burl);
				STORE_INFO(to, TXT_URL_END);
			}
			continue;
		}
		if (strncmp(from, "<http:", 6) == 0
		        || strncmp(from, "<https:", 7) == 0
		        || strncmp(from, "<file:", 6) == 0) {
			//--	<http://www.example.com>
			++from;
			*url = '\0';
			char *pt = url;
			while (*from != MD_ANGLE_BRACKET_R && *from != '\0') {
				*pt = *from;
				++from;
				*(++pt) = '\0';
			}
			MOVE_INFO(to, TXT_LINK_START);
			__backslash_underscore(url, burl);
			MOVE_INFO(to, burl);
			MOVE_INFO(to, TXT_LINK_END);
			STORE_INFO(to, TXT_SEPARATOR);
			STORE_INFO(to, TXT_URL_START);
			MOVE_INFO(to, burl);
			STORE_INFO(to, TXT_URL_END);
			if (*from == MD_ANGLE_BRACKET_R) ++from;
			continue;
		}
		if (strncmp(from, "http:", 5) == 0
		        || strncmp(from, "https:", 6) == 0
		        || strncmp(from, "file:", 5) == 0) {
			//--	http://www.example.com
			*url = '\0';
			char *pt = url;
			while (*from != ' ' && *from != ')' && *from != '\0') {	//not satisfying!!!!
				*pt = *from;
				++from;
				*(++pt) = '\0';
			}
			MOVE_INFO(to, TXT_LINK_START);
			__backslash_underscore(url, burl);
			MOVE_INFO(to, url);
			MOVE_INFO(to, TXT_LINK_END);
			STORE_INFO(to, TXT_SEPARATOR);
			STORE_INFO(to, TXT_URL_START);
			MOVE_INFO(to, burl);
			STORE_INFO(to, TXT_URL_END);
			continue;
		}
		COPY_CHARACTER(to, from);
		++from;
	}
	*to = '\0';
	return count;
}
//------------------------------------------------------------------------------
//-- Convert an arabic number to its roman representation.
//-- Limited to 1000.
static char *_conv_arabic_to_roman(int n, char *res) {
	char tmp[16];
	*tmp = '\0';
	int count = sizeof arabics / sizeof arabics[0];  //This just gets the number of elements
	count--;  //count how the index of the highest value
	int num = n;
	while (num > 0) {
		//if num has a value greater than or equal to the value we are currently looking at
		//subtract that value from num and display the proper symbol for it
		if ( num >= arabics[count] ) {
			strcat(tmp, romans[count]);
			num -= arabics[count];
		}
		//if num doesn't have a value greater than or equal to the value we are currently looking at
		//decrament count we look at a lower value
		else
			count--;
	}
	strcat(tmp, ".");
	sprintf(res, "  %5s ", tmp);
	return res;
}
//------------------------------------------------------------------------------
//-- Convert an arabic number to its letter representation, for list
//-- Limited to 702.
static char *_conv_arabic_to_letters(int n, char *res) {
	--n;
	int q = n / 26;
	int r = n % 26;
	if (q == 0) sprintf(res, "   %c. ",'a' + r);
	else sprintf(res, "  %c%c. ", 'a' + q - 1, 'a' + r);
	return res;
}
//------------------------------------------------------------------------------
static char *_getFirstCharAfterDot( char *line) {
	while (*line != '.') ++line;
	++line;
	while (*line == ' ') ++line;
	return line;
}
//------------------------------------------------------------------------------
//-- Print a line after processing it for attribut (explicite strong_attribut emphasis,
//-- & underline and links) replacemnt. These replacements have no visual impact
//-- on the length of the displayed line.
static void _printLine( printLine_parameters param, char *item ) {
	// Set requested color.
	fprintf(stdout, "%s%s", param.margleft, param.color);
	// If some attributshave been  set in the previous line, then set them again.
	if (strong_attribut) PRINT_ATTRIBUT(escapeSequence.strong_on);
	if (strike_attribut) PRINT_ATTRIBUT(escapeSequence.strike_on);
	if (emphasis_attribut) PRINT_ATTRIBUT(escapeSequence.emphasis_on);
	if (image_attribut) PRINT_ATTRIBUT(escapeSequence.image);
	if (link_attribut) PRINT_ATTRIBUT(escapeSequence.link);
	// Go on....
	char *pt, *centered_item = '\0';
	if (param.alignement == CENTER) {
		centered_item = myalloc(2 * param.linelength);
		size_t k1 = (param.linelength - count_number_of_effective_characters(item)) / 2;
		for (size_t k2 = 0; k2 < k1; k2++) strcat(centered_item, " ");
		strncat(centered_item, item, param.linelength - k1);
		pt = centered_item;
	} else
		pt = item;

	bool backslash = false;
	size_t nb_printedchars = 0;
	while (*pt != '\0') {
		if (backslash) {
			backslash = false;
			fputc(*pt, stdout);
			++nb_printedchars;
			++pt;
			continue;
		}
		if (*pt == '\\') {
			backslash = true;
			++pt;
			continue;
		}
		if (strncmp(pt, MD_STRONG1, 2) == 0 || strncmp(pt, MD_STRONG2, 2) == 0) {
			if (strong_attribut) {
				fprintf(stdout, "%s%s", escapeSequence.attributsoff, param.color);
				if (strike_attribut) PRINT_ATTRIBUT(escapeSequence.strike_on);
				if (emphasis_attribut) PRINT_ATTRIBUT(escapeSequence.emphasis_on);
				if (image_attribut) PRINT_ATTRIBUT(escapeSequence.image);
				if (link_attribut) PRINT_ATTRIBUT(escapeSequence.link);
			} else
				PRINT_ATTRIBUT(escapeSequence.strong_on);
			strong_attribut = !strong_attribut;
			++pt;
			++pt;
			continue;
		}
		if (strncmp(pt, MD_STRIKE, 2) == 0) {
			if (strike_attribut) {
				fprintf(stdout, "%s%s", escapeSequence.attributsoff, param.color);
				if (strong_attribut) PRINT_ATTRIBUT(escapeSequence.strong_on);
				if (emphasis_attribut) PRINT_ATTRIBUT(escapeSequence.emphasis_on);
				if (image_attribut) PRINT_ATTRIBUT(escapeSequence.image);
				if (link_attribut) PRINT_ATTRIBUT(escapeSequence.link);
			} else
				PRINT_ATTRIBUT(escapeSequence.strike_on);
			strike_attribut = ! strike_attribut;
			++pt;
			++pt;
			continue;
		}
		if (strncmp(pt, MD_EMPHASIS1, 1) == 0 || strncmp(pt, MD_EMPHASIS2, 1) == 0) {
			if (emphasis_attribut) {
				fprintf(stdout, "%s%s", escapeSequence.attributsoff, param.color);
				if (strong_attribut) PRINT_ATTRIBUT(escapeSequence.strong_on);
				if (strike_attribut) PRINT_ATTRIBUT(escapeSequence.strike_on);
				if (image_attribut) PRINT_ATTRIBUT(escapeSequence.image);
				if (link_attribut) PRINT_ATTRIBUT(escapeSequence.link);
			} else
				PRINT_ATTRIBUT(escapeSequence.emphasis_on);
			emphasis_attribut = ! emphasis_attribut;
			++pt;
			continue;
		}
		if (strncmp(pt, TXT_IMAGE_START, 2) == 0) {
			PRINT_ATTRIBUT(escapeSequence.image);
			image_attribut = true;
			++pt;
			++pt;
			continue;
		}
		if (strncmp(pt, TXT_IMAGE_END, 2) == 0) {
			fprintf(stdout, "%s%s", escapeSequence.attributsoff, param.color);
			if (strong_attribut) PRINT_ATTRIBUT(escapeSequence.strong_on);
			if (strike_attribut) PRINT_ATTRIBUT(escapeSequence.strike_on);
			if (emphasis_attribut) PRINT_ATTRIBUT(escapeSequence.emphasis_on);
			if (link_attribut) PRINT_ATTRIBUT(escapeSequence.link);
			image_attribut = false;
			++pt;
			++pt;
			continue;
		}
		if (strncmp(pt, TXT_LINK_START, 2) == 0) {
			PRINT_ATTRIBUT(escapeSequence.link);
			link_attribut = true;
			++pt;
			++pt;
			continue;
		}
		if (strncmp(pt, TXT_LINK_END, 2) == 0) {
			fprintf(stdout, "%s%s", escapeSequence.attributsoff, param.color);
			if (strong_attribut) PRINT_ATTRIBUT(escapeSequence.strong_on);
			if (strike_attribut) PRINT_ATTRIBUT(escapeSequence.strike_on);
			if (emphasis_attribut) PRINT_ATTRIBUT(escapeSequence.emphasis_on);
			if (image_attribut) PRINT_ATTRIBUT(escapeSequence.image);
			link_attribut = false;
			++pt;
			++pt;
			continue;
		}
		fputc(*pt, stdout);
		++nb_printedchars;
		++pt;
	}
	// If some attributs have been set, then put off them.
	if (strong_attribut || strike_attribut || emphasis_attribut || image_attribut || link_attribut)
		PRINT_ATTRIBUT(escapeSequence.attributsoff);
	// Simple end of line or set a specific character to the end-of-line colum?
	if (param.complement == WITH_SPACE_COMPLEMENT) {
		for (size_t i = nb_printedchars; i < param.linelength; i++) fprintf(stdout, " ");
	}
	fprintf(stdout, "%s", param.margright);
	if (param.alignement == CENTER)
		myfree(centered_item);
}
//-----------------------------------------------------------------------------
static char *_justify( char *line, size_t requestedlength, size_t currentlength, size_t nbspaces ) {
	size_t todistribute = requestedlength - currentlength;
	char *newline = myalloc(2 * requestedlength);	// in case there is an attribute (2 car) for each character.
	// in case there is an attribute (2 car) for each character
	if (todistribute == 0) {
		strcpy(newline, line);
		return newline;
	}
	size_t frequency = nbspaces / todistribute;
	char *pt = newline;
	size_t f = 0;
	while (*line != '\0') {
		if (*line == ' ' && todistribute != 0) {
			++f;
			if (f == frequency) {
				STORE_INFO(pt, ' ');
				f = 0;
				--todistribute;
			}
		}
		STORE_INFO(pt, *line);
		++line;
	}
	*pt = *line;
	return newline;
}
//-----------------------------------------------------------------------------
static void _printItem(
    size_t linelength,
    char *margleft1stline,
    char *margleft,
    char *color,
    char *item,
    char *margright,
    alignment alignment,
    line_complement type ) {

	printLine_parameters parameters;

	// Initializations
	strong_attribut = emphasis_attribut = strike_attribut = link_attribut = image_attribut = false;
	// Replacing all blockquote characters by a space.
	replace_blockquotes_with_single_spaces(item);
	// Replacing all 'enter caracters by a space.'
	_replace_enter_with_space(item);
	// Replacing all consecutive space characters by 1 space.
	replace_multi_space_with_single_space(item);
	// Replacing image definitions with another more visual form
	char *block1 = replace_images(item);
	if (block1 == NULL) return;
	// Replacing link definitions with another more visual form
	char *block = replace_links(block1);
	myfree(block1);
	if (block == NULL) return;

	parameters.linelength = linelength;
	parameters.margleft = margleft1stline;
	parameters.color = color;
	parameters.margright = margright;
	parameters.alignement = alignment;
	parameters.complement = type;

	// Cut the block into lines before requesting printing.
	size_t nbchars = count_number_of_effective_characters(block);
	if (nbchars <= parameters.linelength)
		_printLine(parameters, block);
	else {
		char *ptstart = block;
		do {
			size_t k, nbspaces;
			char *ptend = give_part_of(ptstart, linelength, &k, &nbspaces);
			char c = *ptend;
			*ptend = '\0';
			if (alignment == JUSTIFY && c != '\0') {
				char *pt = _justify(ptstart, linelength, k, nbspaces);
				_printLine(parameters, pt);
				myfree(pt);
			} else
				_printLine(parameters, ptstart);
			*ptend = c;
			ptstart = ptend;
			if (c == ' ') ++ptstart;
			parameters.margleft = margleft;
		} while (*ptstart != '\0');
	}
	myfree(block);
}
//------------------------------------------------------------------------------
static void reinitialize_list( void ) {
	for (int i = 0; i < MAXDEPTH_LIST; i++) tabIndexOrdList[i] = 0;
	startLastListText = 0;
	*currentAdditionalLeftMargin = '\0';
	idxDepthList = -1;
}
//------------------------------------------------------------------------------
static void _indent( long int k ) {
	if (k == 0 || idxDepthList == -1)
		reinitialize_list();
	else {
		int i;
		if (k > tabDepthList[idxDepthList]) k = tabDepthList[idxDepthList];
		if (k != tabDepthList[idxDepthList]) {
			for (; idxDepthList >= 0; idxDepthList--) {
				if (k == tabDepthList[idxDepthList] || k > tabDepthList[idxDepthList]) break;
			}
		}
		*currentAdditionalLeftMargin = '\0';
		for (int j = 0; j <= idxDepthList; j++)
			strcat(currentAdditionalLeftMargin, LEFTMARGINLIST);
	}
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
//-- Compute the "visual" size of the string marginleftblockquote that contains
//-- graphical characters
size_t sizeMLBQ( void ) {
	return (strlen(marginleftblockquote) / strlen(LEFTMARGINBLOCKQUOTE)) * LENGTH_LMBQ;
}
//------------------------------------------------------------------------------
char *add_return_at_the_end( char *str ) {
	char *dest = myalloc(strlen(str) * 2);
	strcpy(dest, str);
	if (dest[strlen(dest) - 1] != '\n') strcat(dest, "\n");
	return(dest);
}
//------------------------------------------------------------------------------
char *replace_tabs_with_spaces( char *str ) {
	char *dest = myalloc(strlen(str) * SAFETY_FACTOR);
	char *ptdest = dest;
	while (*str != '\0') {
		if (*str == '\t') {
			for (int i = 0; i < 4; i++) STORE_INFO(ptdest, ' ');
		} else  STORE_INFO(ptdest, *str);
		++str;
	}
	*ptdest = *str;
	return dest;
}
//-----------------------------------------------------------------------------
size_t count_number_of_effective_characters( char *str )  {
	size_t len = 0;
	bool backslash = false;
	while (*str != '\0') {
		if (backslash) {
			backslash = false;
			++len;
			++str;
			continue;
		}
		if (*str == '\\') {
			backslash = true;
			++str;
			continue;
		}
		if (strncmp(str, MD_STRONG1, 2) == 0
		        || strncmp(str, MD_STRONG2, 2) == 0
		        || strncmp(str, MD_STRIKE, 2) == 0
		        || strncmp(str, TXT_IMAGE_START, 2) == 0
		        || strncmp(str, TXT_IMAGE_END, 2) == 0
		        || strncmp(str, TXT_LINK_START, 2) == 0
		        || strncmp(str, TXT_LINK_END, 2) == 0) {
			str = str + 2;
			continue;
		}
		if (strncmp(str, MD_EMPHASIS1, 1) == 0
		        || strncmp(str, MD_EMPHASIS2, 1) == 0) {
			++str;
			continue;
		}
		++len;
		++str;
	}
	return len;
}
//------------------------------------------------------------------------------
size_t count_characters_out_escaped_codes( char *str ) {
	unsigned char *s = (void *)str;
	size_t non_escape_chars = 0;
	size_t non_printable_nbchars = 0;
	int state = 0;
	while (1) {
		if (*s == '\0') break;
		switch (state) {
		case 0:
			if (*s == '\033') state = 1;
			else if (*s >= 128) ++non_printable_nbchars;
			else ++non_escape_chars;
			break;
		case 1:
			if (*s == '[') state = 2;
			break;
		case 2:
			if (*s >= '0' && *s <= '9') state = 3;
			else if (*s == 'm') state = 0;
			break;
		case 3:
			if (*s >= '0' && *s <= '9') state = 4;
			else if (*s == ';') state = 2;
			break;
		case 4:
			if (*s == 'm') state = 0;
			else if (*s == ';') state = 2;
			break;
		}
		++s;
	}
	return non_escape_chars + non_printable_nbchars / 3;
}

size_t compute_index_out_escaped_codes( char *str, size_t idx) {
	unsigned char *s = (void *)str;
	size_t non_escape_chars = 0;
	size_t escape_chars = 0;
	size_t non_printable_nbchars = 0;
	int state = 0;
	while (1) {
		if (idx == non_escape_chars + non_printable_nbchars / 3 && (non_printable_nbchars % 3) == 0)
			break;
		if (*s == '\0')
			break;
		switch (state) {
		case 0:
			if (*s == '\033') {
				++escape_chars;
				state = 1;
			} else if (*s >= 128) ++non_printable_nbchars;
			else ++non_escape_chars;
			break;
		case 1:
			if (*s == '[') {
				++escape_chars;
				state = 2;
			}
			break;
		case 2:
			if (*s >= '0' && *s <= '9') {
				++escape_chars;
				state = 3;
			} else if (*s == 'm') {
				++escape_chars;
				state = 0;
			}
			break;
		case 3:
			if (*s >= '0' && *s <= '9') {
				++escape_chars;
				state = 4;
			} else if (*s == ';') {
				++escape_chars;
				state = 2;
			}
			break;
		case 4:
			if (*s == 'm') {
				++escape_chars;
				state = 0;
			} else if (*s == ';') {
				++escape_chars;
				state = 2;
			}
			break;
		}
		++s;
	}
	return non_escape_chars + escape_chars + non_printable_nbchars;
}
//-----------------------------------------------------------------------------
char *give_part_of( char *start, size_t requestedlength, size_t *actuallength, size_t *nbspaces ) {
	*actuallength = *nbspaces = 0;
	bool backslash = false;
	size_t indexoflastspace = 0;
	char *addroflastspace = NULL;
	// Looking for space near the max limit.
	while (*start != '\0') {
		if (backslash) {
			backslash = false;
			++*actuallength;
			++start;
			continue;
		}
		if (*start == '\\') {
			backslash = true;
			++start;
			continue;
		}
		if (strncmp(start, MD_STRONG1, 2) == 0
		        || strncmp(start, MD_STRONG2, 2) == 0
		        || strncmp(start, MD_STRIKE, 2) == 0
		        || strncmp(start, TXT_IMAGE_START, 2) == 0
		        || strncmp(start, TXT_IMAGE_END, 2) == 0
		        || strncmp(start, TXT_LINK_START, 2) == 0
		        || strncmp(start, TXT_LINK_END, 2) == 0) {
			start = start + 2;
			continue;
		}
		if (strncmp(start, MD_EMPHASIS1, 1) == 0
		        || strncmp(start, MD_EMPHASIS2, 1) == 0) {
			++start;
			continue;
		}
		if (*start == ' ') {
			indexoflastspace = *actuallength;
			addroflastspace = start;
			++*nbspaces;
		}
		++*actuallength;
		++start;
		if (*actuallength == requestedlength) break;
	}
	if (*start == '\0') return start;				// Line  shorter than the demand
	if (indexoflastspace == 0) return start; 		// Right length but no space available
	*actuallength = indexoflastspace;
	--*nbspaces;
	return addroflastspace;
}
//------------------------------------------------------------------------------
void replace_multi_space_with_single_space( char *str ) {
	char *dest = str;  /* Destination to copy to */
	// While we're not at the end of the string, loop...
	while (*str != '\0') {
		// Loop while the current character is a space, AND the next character is a space
		while (*str == ' ' && *(str + 1) == ' ') str++;  /* Just skip to next character */
		// Copy from the "source" string to the "destination" string,
		// while advancing to the next character in both
		*dest++ = *str++;
	}
	// Make sure the string is properly terminated
	*dest = '\0';
}
//------------------------------------------------------------------------------
void replace_blockquotes_with_single_spaces( char *str ) {
	bool beginofline = true;
	while (*str != '\0') {
		if (*str == '\n') beginofline = true;
		else if (*str == MD_BLOCKQUOTE && beginofline) *str = ' ';
		else if (*str != MD_BLOCKQUOTE && *str != ' ' && beginofline) beginofline = false;
		++str;
	}
}//------------------------------------------------------------------------------
char *replace_links( char *str ) {
	char *dest = myalloc(strlen(str) * SAFETY_FACTOR);
	(void)_replace_links(str, dest);
	return dest;
}
//------------------------------------------------------------------------------
char *replace_images( char *str ) {
	char *dest = myalloc(strlen(str) * SAFETY_FACTOR);
	(void)_replace_images(str, dest);
	return dest;
}
//------------------------------------------------------------------------------
void printEmpty( void ) {
	fprintf(stdout, "%s%s\n", LEFTMARGIN, marginleftblockquote);
}
//------------------------------------------------------------------------------
void printText( char *item ) {
	char *ptitem = mdGetFirstChar(item);
	long int k = ptitem - item;
	_indent(k);
	sprintf(marginleft, "%s%s%s", LEFTMARGIN, marginleftblockquote, currentAdditionalLeftMargin);
	sprintf(marginright, "%s\n", RIGHTMARGIN);
	_printItem(nbCOLS - (LENGTH_LM + strlen(currentAdditionalLeftMargin) + sizeMLBQ() + LENGTH_RM),
	           marginleft, marginleft, (char *)NO_COLOR, ptitem, marginright, text_alignment, NO_SPACE_COMPLEMENT);
}
//-----------------------------------------------------------------------------
#define PRINT_FRAME_HEADER1(leftcorner, rightcorner)	\
		fprintf(stdout, "%s%s%s", LEFTMARGIN, marginleftblockquote, leftcorner); \
		for (size_t i = LENGTH_LM + sizeMLBQ() + LENGTH_RM + 2; i < nbCOLS; i++) fprintf(stdout, "%s", HEADER_HORIZONTAL); \
		fprintf(stdout, "%s\n", rightcorner)

void printHeader1( char *item ) {
	sprintf(marginleft, "%s%s%s", LEFTMARGIN, marginleftblockquote, HEADER_VERTICAL_LEFT);
	sprintf(marginright, "%s%s%s\n", escapeSequence.attributsoff, HEADER_VERTICAL_RIGHT, RIGHTMARGIN);

	if (previousMarkdown != HEADER1 && previousMarkdown != HEADER2 &&previousMarkdown != HEADER3
	        && previousMarkdown != HEADER4 && previousMarkdown != HEADER5 && previousMarkdown != HEADER6
	        && previousMarkdown != EMPTY)
		fprintf(stdout, "%s%s\n", LEFTMARGIN, marginleftblockquote);
	PRINT_FRAME_HEADER1(HEADER_TOPLEFTCORNER, HEADER_TOPRIGHTCORNER);
	_printItem(nbCOLS - (LENGTH_LM  + sizeMLBQ() + strlen(HEADER_VERTICAL_RIGHT) + LENGTH_RM),
	           marginleft, marginleft, escapeSequence.h1, item, marginright, CENTER, WITH_SPACE_COMPLEMENT);
	PRINT_FRAME_HEADER1(HEADER_BOTTOMLEFTCORNER, HEADER_BOTTOMRIGHTCORNER);
	fprintf(stdout, "%s%s\n", LEFTMARGIN, marginleftblockquote);

	reinitialize_list();
}
//-----------------------------------------------------------------------------
void printHeader2( char *item ) {
	sprintf(marginleft, "%s%s%s", LEFTMARGIN, marginleftblockquote, LEFTMARGINHEADER2);
	sprintf(marginright, "%s%s\n", escapeSequence.attributsoff, RIGHTMARGIN);

	if (previousMarkdown != HEADER1 && previousMarkdown != HEADER2 &&previousMarkdown != HEADER3
	        && previousMarkdown != HEADER4 && previousMarkdown != HEADER5 && previousMarkdown != HEADER6
	        && previousMarkdown != EMPTY)
		fprintf(stdout, "%s%s\n", LEFTMARGIN, marginleftblockquote);
	_printItem(nbCOLS - (LENGTH_LM  + sizeMLBQ() + LENGTH_LMH2),
	           marginleft, marginleft, escapeSequence.h2, item, marginright, text_alignment, NO_SPACE_COMPLEMENT);
	fprintf(stdout, "%s%s\n", LEFTMARGIN, marginleftblockquote);

	reinitialize_list();
}
//-----------------------------------------------------------------------------
void printHeader3( char *item ) {
	sprintf(marginleft, "%s%s%s", LEFTMARGIN, marginleftblockquote, LEFTMARGINHEADER3);
	sprintf(marginright, "%s%s\n", escapeSequence.attributsoff, RIGHTMARGIN);

	if (previousMarkdown != HEADER1 && previousMarkdown != HEADER2 &&previousMarkdown != HEADER3
	        && previousMarkdown != HEADER4 && previousMarkdown != HEADER5 && previousMarkdown != HEADER6
	        && previousMarkdown != EMPTY)
		fprintf(stdout, "%s%s\n", LEFTMARGIN, marginleftblockquote);
	_printItem(nbCOLS - (LENGTH_LM  + sizeMLBQ() + LENGTH_LMH3 + LENGTH_RM),
	           marginleft, marginleft, escapeSequence.h3, item, marginright, text_alignment, NO_SPACE_COMPLEMENT);
	fprintf(stdout, "%s%s\n", LEFTMARGIN, marginleftblockquote);

	reinitialize_list();
}
//-----------------------------------------------------------------------------
void printHeader4( char *item ) {
	sprintf(marginleft, "%s%s%s", LEFTMARGIN, marginleftblockquote, LEFTMARGINHEADER4);
	sprintf(marginright, "%s%s\n", escapeSequence.attributsoff, RIGHTMARGIN);

	if (previousMarkdown != HEADER1 && previousMarkdown != HEADER2 &&previousMarkdown != HEADER3
	        && previousMarkdown != HEADER4 && previousMarkdown != HEADER5 && previousMarkdown != HEADER6
	        && previousMarkdown != EMPTY)
		fprintf(stdout, "%s%s\n", LEFTMARGIN, marginleftblockquote);
	_printItem(nbCOLS - (LENGTH_LM  + sizeMLBQ() + LENGTH_LMH4 + LENGTH_RM),
	           marginleft, marginleft, escapeSequence.h4, item, marginright, text_alignment, NO_SPACE_COMPLEMENT);
	fprintf(stdout, "%s%s\n", LEFTMARGIN, marginleftblockquote);

	reinitialize_list();
}
//-----------------------------------------------------------------------------
void printHeader5( char *item ) {
	sprintf(marginleft, "%s%s%s", LEFTMARGIN, marginleftblockquote,  LEFTMARGINHEADER5);
	sprintf(marginright, "%s%s\n", escapeSequence.attributsoff, RIGHTMARGIN);

	if (previousMarkdown != HEADER1 && previousMarkdown != HEADER2 &&previousMarkdown != HEADER3
	        && previousMarkdown != HEADER4 && previousMarkdown != HEADER5 && previousMarkdown != HEADER6
	        && previousMarkdown != EMPTY)
		fprintf(stdout, "%s%s\n", LEFTMARGIN, marginleftblockquote + LENGTH_RM);
	_printItem(nbCOLS - (LENGTH_LM  + sizeMLBQ() + LENGTH_LMH5),
	           marginleft, marginleft, escapeSequence.h5, item, marginright, text_alignment, NO_SPACE_COMPLEMENT);
	fprintf(stdout, "%s%s\n", LEFTMARGIN, marginleftblockquote);

	reinitialize_list();
}
//-----------------------------------------------------------------------------
void printHeader6( char *item ) {
	sprintf(marginleft, "%s%s%s", LEFTMARGIN, marginleftblockquote, LEFTMARGINHEADER6);
	sprintf(marginright, "%s%s\n", escapeSequence.attributsoff, RIGHTMARGIN);

	if (previousMarkdown != HEADER1 && previousMarkdown != HEADER2 &&previousMarkdown != HEADER3
	        && previousMarkdown != HEADER4 && previousMarkdown != HEADER5 && previousMarkdown != HEADER6
	        && previousMarkdown != EMPTY)
		fprintf(stdout, "%s%s\n", LEFTMARGIN, marginleftblockquote);
	_printItem(nbCOLS - (LENGTH_LM  + sizeMLBQ() + LENGTH_LMH6 + LENGTH_RM),
	           marginleft, marginleft, escapeSequence.h6, item, marginright, text_alignment, NO_SPACE_COMPLEMENT);
	fprintf(stdout, "%s%s\n", LEFTMARGIN, marginleftblockquote);

	reinitialize_list();
}
//-----------------------------------------------------------------------------
void printHline( char *item ) {
	char *ptitem = mdGetFirstChar(item);
	long int k = ptitem - item;
	_indent(k);

	fprintf(stdout, "%s%s%s", LEFTMARGIN, marginleftblockquote, currentAdditionalLeftMargin);
	for (size_t i = LENGTH_LM+ strlen(currentAdditionalLeftMargin) + sizeMLBQ() + LENGTH_RM; i < nbCOLS; i++)
		fprintf(stdout, HORIZONTAL_LINE);
	fprintf(stdout, "%s\n", RIGHTMARGIN);

	if (k == 0) reinitialize_list();
}
//-----------------------------------------------------------------------------
void printList( char *item, listtype type ) {
	char index[64];
	char *ptitem = mdGetFirstChar(item);
	int i;
	const char *ptdot;
	long int k = ptitem - item;

	if (k == 0) {
		idxDepthList = i = 0;
		tabDepthList[idxDepthList] = 0;
	} else {
		if (k > tabDepthList[idxDepthList]) {
			if (idxDepthList < MAXDEPTH_LIST) {
				++idxDepthList;
				tabDepthList[idxDepthList] = (int)k;
			}
			i = idxDepthList;
		} else {
			if (k == tabDepthList[idxDepthList]) {
				i = idxDepthList;
			} else {
				for (i = idxDepthList - 1; i >= 0; i--) {
					if (k == tabDepthList[i] || k > tabDepthList[i]) break;
				}
			}
		}
	}
	if (type == NONORDERED) {
		switch (i) {
		case 0:
			ptdot = DOT1;
			break;
		case 1:
			ptdot = DOT2;
			break;
		case 2:
			ptdot = DOT3;
			break;
		default:
			ptdot = DOT4;
			break;
		}
		ptitem = mdGetFirstChar(item) + 2;
	} else {
		++tabIndexOrdList[i];
		switch (i) {
		case 0:
			sprintf(index, "  %d. ", tabIndexOrdList[i]);
			ptdot = index;
			break;
		case 1:
			ptdot = _conv_arabic_to_roman(tabIndexOrdList[i], index);
			break;
		default:
			ptdot = _conv_arabic_to_letters(tabIndexOrdList[i], index);
			break;
		}
		ptitem = _getFirstCharAfterDot(item);
	}

	sprintf(marginleftfirstline, "%s%s", LEFTMARGIN, marginleftblockquote);
	*currentAdditionalLeftMargin = '\0';
	for (k = 0; k < i; k++) {
		strcat(marginleftfirstline, LEFTMARGINLIST);
		strcat(currentAdditionalLeftMargin, LEFTMARGINLIST);
	}
	strcat(marginleftfirstline, ptdot);
	strcat(currentAdditionalLeftMargin, LEFTMARGINLIST);

	sprintf(marginleft, "%s%s%s", LEFTMARGIN, marginleftblockquote, currentAdditionalLeftMargin);
	sprintf(marginright, "%s\n", RIGHTMARGIN);

	startLastListText = ptitem - item;
	_printItem(nbCOLS - (LENGTH_LM + sizeMLBQ() + strlen(currentAdditionalLeftMargin) + 1),
	           marginleftfirstline, marginleft, (char *)NO_COLOR, ptitem, marginright, text_alignment, NO_SPACE_COMPLEMENT);
}
//-----------------------------------------------------------------------------
void generate_marginleftblockquote( int number ) {
	if (number > MAXBLOCKQUOTES) number = MAXBLOCKQUOTES;
	*marginleftblockquote = '\0';
	for (int i = 0; i < number; i++)
		strcat(marginleftblockquote, LEFTMARGINBLOCKQUOTE);
}
//-----------------------------------------------------------------------------
void allocText( void ) {
	marginleft = myalloc(256);
	marginright = myalloc(256);
	marginleftfirstline = myalloc(256);
	currentAdditionalLeftMargin = myalloc(256);
	marginleftblockquote = myalloc(256);
}
//-----------------------------------------------------------------------------
void freeText( void ) {
	myfree(marginleft);
	myfree(marginright);
	myfree(marginleftfirstline);
	myfree(currentAdditionalLeftMargin);
	myfree(marginleftblockquote);
}
//-----------------------------------------------------------------------------