//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: willy
// Linux Markdown text-formatting program producing output suitable for simple
// fixed-width terminal windows.
//------------------------------------------------------------------------------
#ifndef WILLY_ESCAPE_H
#define WILLY_ESCAPE_H
//------------------------------------------------------------------------------
// TYPEDEFS
//------------------------------------------------------------------------------
typedef struct {
	char *h1;
	char *h2;
	char *h3;
	char *h4;
	char *h5;
	char *h6;
	char *code;
	char *link;
	char *link_reverse;
	char *image;
	char *image_reverse;
	char *attributsoff;
	char *strong_on;
	char *emphasis_on;
	char *strike_on;
	char *reverse_on;
	char *reversestrong_on;
} ansiescapesequence;
//------------------------------------------------------------------------------
// FUNCTIONS
//------------------------------------------------------------------------------
extern void	setEscapeSequence( void );
extern void freeEscapeSequence( void );
//------------------------------------------------------------------------------
#endif	// WILLY_ESCAPE_H
