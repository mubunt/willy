//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: willy
// Linux Markdown text-formatting program producing output suitable for simple
// fixed-width terminal windows.
//------------------------------------------------------------------------------
#ifndef WILLY_TEXT_H
#define WILLY_TEXT_H
//------------------------------------------------------------------------------
// TYPEDEFS
//------------------------------------------------------------------------------
typedef enum { ORDERED, NONORDERED } listtype;
//------------------------------------------------------------------------------
// FUNCTIONS
//------------------------------------------------------------------------------
extern void		printText( char * );
extern void		printHeader1( char * );
extern void		printHeader2( char * );
extern void		printHeader3( char * );
extern void		printHeader4( char * );
extern void		printHeader5( char * );
extern void		printHeader6( char * );
extern void		printHline( char * );
extern void		printList( char *, listtype );
extern void		printEmpty( void );

extern void		generate_marginleftblockquote( int );
extern void		replace_multi_space_with_single_space( char * );
extern void		replace_blockquotes_with_single_spaces( char * );
extern char 	*replace_tabs_with_spaces( char * );
extern char		*add_return_at_the_end( char * );
extern char 	*replace_links( char * );
extern char 	*replace_images( char * );
extern size_t	count_number_of_effective_characters( char * );
extern char 	*give_part_of( char *, size_t, size_t *, size_t * );
extern size_t	sizeMLBQ( void );
extern void		allocText( void );
extern void		freeText( void );
extern size_t	count_characters_out_escaped_codes( char * );
extern size_t 	compute_index_out_escaped_codes( char *, size_t );
//------------------------------------------------------------------------------
#endif	// WILLY_TEXT_H
