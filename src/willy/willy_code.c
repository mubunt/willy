//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: willy
// Linux Markdown text-formatting program producing output suitable for simple
// fixed-width terminal windows.
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>
#include <stdbool.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "willy_common.h"
#include "willy_text.h"
#include "willy_drawingchars.h"
#include "willy_escape.h"
#include "willy_markdown.h"
#include "willy_configuration.h"
#include "willy_funcs.h"
#include "willy_code.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// EXTERNAL VARIABLES
//------------------------------------------------------------------------------
extern size_t 				nbCOLS;					// Number of columns of the terminal
extern long int 			startLastListText;		// Index of the last text of a bullet of a list
extern bool 				syntax_coloring;		// Indicator if syntax coloring available
extern unsigned int 		display_configuration;	// Configuration as defined in configuration file
extern ansiescapesequence	escapeSequence;			// ANSI Escape sequence for test attributs and colors
extern  char 				*marginleftblockquote;
extern char 				*currentAdditionalLeftMargin;
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void _rest_of_the_line_of_code( size_t start, size_t end ) {
	for (size_t i = start; i < end; i++) fprintf(stdout, " ");
	fprintf(stdout, "%s%s\n", CODE_VERTICAL_RIGHT, RIGHTMARGIN);
}

static void _rest_of_the_line_of_code2( size_t start, size_t end ) {
	for (size_t i = start; i < end; i++) fprintf(stdout, " ");
	fprintf(stdout, "%s%s\n", escapeSequence.attributsoff, RIGHTMARGIN);
}
//------------------------------------------------------------------------------
static void _syncol_create_temporary_file( char *filetmp, char *text, code type ) {
	FILE *tmp_fd = NULL;
	if (NULL == file_gettemporaryfilename(filetmp, SYNTAX)) {
		myexit(EXIT_FAILURE);
	}
	if (NULL == (tmp_fd = fopen(filetmp, "w"))) {
		error("%s", "Cannot open temporary output file for coloring code.");
		file_unlinktemporaryfile(filetmp);
		myexit(EXIT_FAILURE);
	}
	char *pt;
	if (type == CODEWITHKWORD) pt = text;
	else {
		pt = text + startLastListText;
		while (*pt == ' ') ++pt;
	}
	char *ptstart = pt;
	while (*pt != '\0') {
		if (*pt == '\n') {
			*pt = '\0';
			fprintf(tmp_fd, "%s\n", ptstart);
			++pt;
			if (*pt != '\0') {
				if (type != CODEWITHKWORD) pt += startLastListText;
				while (*pt == ' ' || *pt == MD_BLOCKQUOTE) ++pt;
			}
			ptstart = pt;
		} else
			++pt;
	}
	if (*ptstart != '\0') fprintf(tmp_fd, "%s\n", ptstart);
	fclose(tmp_fd);
}
//------------------------------------------------------------------------------
// MACROS CODE DEFINITIONS
//-----------------------------------------------------------------------------
#define TOPCODEPRINT()		if (CHECKINGBIT(display_configuration, CONFIGURATION_CODE_DEFAULT)) { \
								fprintf(stdout, "%s%s%s%s", \
									LEFTMARGIN, marginleftblockquote, currentAdditionalLeftMargin, CODE_TOPLEFTCORNER); \
								for (size_t i = LENGTH_LM + sizeMLBQ() + strlen(currentAdditionalLeftMargin); i <= pos; i++) \
									fprintf(stdout, "%s", CODE_HORIZONTAL); \
								fprintf(stdout, "%s\n", CODE_TOPRIGHTCORNER); \
							} else { \
								fprintf(stdout, "\n"); \
							}
#define BOTCODEPRINT()		if (CHECKINGBIT(display_configuration, CONFIGURATION_CODE_DEFAULT)) { \
								fprintf(stdout, "%s%s%s%s", \
									LEFTMARGIN, marginleftblockquote, currentAdditionalLeftMargin, CODE_BOTTOMLEFTCORNER); \
								for (size_t i = LENGTH_LM + sizeMLBQ() + strlen(currentAdditionalLeftMargin); i <= pos; i++) \
									fprintf(stdout, "%s", CODE_HORIZONTAL); \
								fprintf(stdout, "%s\n", CODE_BOTTOMRIGHTCORNER); \
							} else { \
								fprintf(stdout, "\n"); \
							}
#define CODEPRINT(s)		if (CHECKINGBIT(display_configuration, CONFIGURATION_CODE_DEFAULT)) { \
								fprintf(stdout, "%s%s%s%s%s%s%s%s", \
				        			LEFTMARGIN, marginleftblockquote, currentAdditionalLeftMargin, \
				        			CODE_VERTICAL_LEFT, LEFTCODE_MARGIN, \
				        			escapeSequence.code, s, escapeSequence.attributsoff); \
								_rest_of_the_line_of_code(count_characters_out_escaped_codes(s) + from + 1, pos); \
							} else { \
								fprintf(stdout, "%s%s%s%s%s%s%s", \
				        			LEFTMARGIN, marginleftblockquote, currentAdditionalLeftMargin, \
				        			escapeSequence.code, LEFTCODE_MARGIN, CODEBACKGROUND_MARGIN, s); \
								_rest_of_the_line_of_code2(count_characters_out_escaped_codes(s) + from + 1, pos); \
							}
#define CODEPRINTSYNTAX(s)	fprintf(stdout, "%s%s%s%s%s%s%s", \
				        		LEFTMARGIN, marginleftblockquote, currentAdditionalLeftMargin, \
				        		CODE_VERTICAL_LEFT, LEFTCODE_MARGIN, \
				        		s, escapeSequence.attributsoff); \
							_rest_of_the_line_of_code(count_characters_out_escaped_codes(s) + from + 1, pos)

void printCode( char *item, code type, char *language ) {
	char *pt = add_return_at_the_end(item);
	char *code = replace_tabs_with_spaces(pt);
	myfree(pt);

	size_t pos = nbCOLS - LENGTH_RM - LENGTH_CODE_VERTICAL_RIGHT;
	size_t from = LENGTH_LM + sizeMLBQ() + strlen(currentAdditionalLeftMargin) + LENGTH_CODE_VERTICAL_LEFT + strlen(LEFTCODE_MARGIN) - 1;
	size_t maxlength = pos - from - 1;

	if (syntax_coloring) {
		char filetmp[TMPSIZE];
		_syncol_create_temporary_file(filetmp, code, type);
		char command[1024];
		if (*language == '\0')
			sprintf(command, SYNTAXCOLORINGCMDNOLANG, SYNTAXCOLORING, filetmp);
		else
			sprintf(command, SYNTAXCOLORINGCMD, SYNTAXCOLORING, filetmp, language);
		FILE *pf;
		if (! (pf = popen(command,"r"))) {
			error("%s", "Cannot open temporary output file for coloring code.");
			file_unlinktemporaryfile(filetmp);
			myexit(EXIT_FAILURE);
		}
		char buffer[1024];
		TOPCODEPRINT();
		while (NULL != fgets(buffer, sizeof(buffer), pf)) {
			if (buffer[strlen(buffer) - 1] == '\n') buffer[strlen(buffer) - 1] = '\0';
			if (count_characters_out_escaped_codes(buffer) > maxlength)
				buffer[ compute_index_out_escaped_codes(buffer, maxlength)] = '\0';
			CODEPRINTSYNTAX(buffer);
		}
		BOTCODEPRINT();
		(void) pclose(pf);
		file_unlinktemporaryfile(filetmp);
	} else {
		if (type == CODEWITHKWORD) pt = code;
		else {
			pt = code + startLastListText;
			while (*pt == ' ') ++pt;
		}
		char *ptstart = pt;
		TOPCODEPRINT();
		while (*pt != '\0') {
			if (*pt == '\n') {
				*pt = '\0';
				while (count_characters_out_escaped_codes(ptstart) > maxlength) {
					size_t k = compute_index_out_escaped_codes(ptstart, maxlength);
					char c = *(ptstart + k);
					*(ptstart + k) = '\0';
					CODEPRINT(ptstart);
					ptstart = ptstart + k;
					*ptstart = c;
				}
				CODEPRINT(ptstart);
				++pt;
				if (*pt != '\0') {
					if (type != CODEWITHKWORD) pt += startLastListText;
					while (*pt == ' ' || *pt == MD_BLOCKQUOTE) ++pt;
				}
				ptstart = pt;
			} else
				++pt;
		}
		if (*ptstart != '\0') {
			CODEPRINT(ptstart);
		}
		BOTCODEPRINT();
	}
	myfree(code);
}
//-----------------------------------------------------------------------------
