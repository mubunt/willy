//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: willy
// Linux Markdown text-formatting program producing output suitable for simple
// fixed-width terminal windows.
//------------------------------------------------------------------------------
#ifndef WILLY_CONFIGURATION_H
#define WILLY_CONFIGURATION_H
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define CONFIGURATION_FILE				".willyrc"

#define BLACK 							"black"
#define RED 							"red"
#define GREEN 							"green"
#define YELLOW 							"yellow"
#define BLUE 							"blue"
#define MAGENTA 						"magenta"
#define CYAN 							"cyan"
#define WHITE 							"white"

#define BOLD 							"bold"
#define BOLDUNDERLINE 					"boldunderline"
#define UNDERLINE 						"underline"
#define ITALIC 							"italic"

#define NORMAL 							"normal"

#define ON 								"on"
#define OFF 							"off"

#define DEFAULT 						"default"
#define HORIZON 						"horizon"
#define ZEBRA 							"zebra"
#define REVERSE 						"reverse"
//--- Display configuration ----------------------------------------------------
#define CONFIGURATION_CODE_DEFAULT			0x0				// Bit 0 - Code block: default (frame)
#define CONFIGURATION_CODE_FOREGROUND		0x1				// Bit 1 - Code block: foreground color and no frame
#define CONFIGURATION_TABLE_DEFAULT			0x2				// Bit 2 - Table: default (frame)
#define CONFIGURATION_TABLE_HORIZON			0x3				// Bit 3 - Table: horizontal (no vertical edges)
#define CONFIGURATION_TABLE_ZEBRA			0x4				// Bit 4 - Table: zebra
#define CONFIGURATION_TABLE_REVERSE			0x5				// Bit 5 - Table: reverse
#define CONFIGURATION_SYNTAXHIGHLIGHTING	0xf				// Bit 15 - Syntax highlighting by default

#define CHECKINGBIT(config, x)			(config >> x) & 1U
#define SETBIT(config, x)				config |= 1U << x
#define CLEARBIT(config, x)				config &= ~(1U << x)
//------------------------------------------------------------------------------
// TYPEDEF DEFINITIONS
//------------------------------------------------------------------------------
typedef struct {
	// [general] section
	char *syntaxhighlighting;
	// [header1] section
	char *h1_foregroundcolor;
	char *h1_attribut;
	// [header2] section
	char *h2_foregroundcolor;
	char *h2_attribut;
	// [header3] section
	char *h3_foregroundcolor;
	char *h3_attribut;
	// [header4] section
	char *h4_foregroundcolor;
	char *h4_attribut;
	// [header5] section
	char *h5_foregroundcolor;
	char *h5_attribut;
	// [header6] section
	char *h6_foregroundcolor;
	char *h6_attribut;
	// [code] section
	char *code_foregroundcolor;
	char *code_backgroundcolor;
	char *code_attribut;
	// [link] section
	char *link_foregroundcolor;
	char *link_attribut;
	// [image] section
	char *image_foregroundcolor;
	char *image_attribut;
	// [table] section
	char *table_class;
} configuration;
//------------------------------------------------------------------------------
// FUNCTIONS
//------------------------------------------------------------------------------
extern bool get_configuration( char *, unsigned int *, configuration * );
extern unsigned int init_configuration( configuration * ) ;
extern void free_configuration( configuration * );
//------------------------------------------------------------------------------
#endif	// WILLY_CONFIGURATION_H
