//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: willy
// Linux Markdown text-formatting program producing output suitable for simple
// fixed-width terminal windows.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <fcntl.h>
#include <pwd.h>
#include <signal.h>
#include <libgen.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <linux/limits.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "willy_common.h"
#include "willy_text.h"
#include "willy_code.h"
#include "willy_table.h"
#include "willy_markdown.h"
#include "willy_configuration.h"
#include "willy_feh.h"
#include "willy_report.h"
#include "willy_funcs.h"
#include "willy_escape.h"
#include "willy_cmdline.h"
#include "pager.h"
//------------------------------------------------------------------------------
// EXPORTABLE VARIABLES
//------------------------------------------------------------------------------
int									_temporary_files		= 0;		// Number of temporary files still to be deleted
int									_allocated_memory		= 0;		// Number of allocated memories still to be deleted
size_t								nbCOLS 					= 0;		// Number of columns of the terminal
unsigned int 						display_configuration	= 0;		// Configuration as defined in configuration file
long int 							startLastListText		= 0;		// Index of the last text of a bullet of a list
alignment 							text_alignment			= LEFT;		// Default alignment (LEFT, JUSTIFY)
lexical 							previousMarkdown		= EMPTY;	// Previous mak-down directive
bool 							syntax_coloring			= false;	// Indicator if syntax coloring available
char 								pathofmdfile[PATH_MAX];				// Path of the current user's MD file
configuration						markdown_configuration;				// user-definied configuration
ansiescapesequence					escapeSequence 			=	{		// ANSI Escape sequence for test attributs and colors
	NULL, NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL, NULL,
};
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static struct gengetopt_args_info	args_info;							// Gengetopt argument structure
static int 							ex_stdout				= -1;		// stdout backup
static int 							outfd					= -1;		// output file descriptor
static bool 						temporaryout			= false;
static int							return_value 			= EXIT_SUCCESS;
static long int 					check 					= 0;
static char							banner[256];
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static bool _getTerminalSize( size_t *cols ) {
	struct winsize w;
	if (0 != ioctl(0, TIOCGWINSZ, &w)) return false;
	*cols = w.ws_col;
	return true;
}
//------------------------------------------------------------------------------
static void _getCodeLanguage(char *line, char *language) {
	*language = '\0';
	char *pt = line + strlen(MD_CODE);
	while (*pt == ' ') ++pt;
	while (*pt != '\0' && *pt != '\n') {
		*language = (char)tolower(*pt);
		++language;
		++pt;
	}
	*language = '\0';
}
//------------------------------------------------------------------------------
static void _checker( void ) {
	fprintf(stdout, "\n");
	if (_temporary_files)
		if (_temporary_files > 0)
			debug("Temporary files...... there are still %d files created and not deleted", _temporary_files);
		else
			debug("Temporary files...... there were %d deleted files that were not created!", abs(_temporary_files));
	else
		debug("%s", "Temporary files...... all created files have been deleted.");
	if (_allocated_memory)
		if (_allocated_memory > 0)
			debug("Allocated memories... there are still %d memory blocks allocated and not freed", _allocated_memory);
		else
			debug("Allocated memories... there were %d freed memory blocks that were not allocated!", abs(_allocated_memory));
	else
		debug("%s", "Allocated memories... all allocated memory blocks have been freed.");
	fprintf(stdout, "\n");
}
//------------------------------------------------------------------------------
// From  Matteo Italia: https://stackoverflow.com/questions/41230547/check-if-program-is-installed-in-c
static bool _can_run_command(const char *cmd) {
	if(strchr(cmd, '/')) {
		// if cmd includes a slash, no path search must be performed,
		// go straight to checking if it's executable
		return access(cmd, X_OK)==0;
	}
	const char *path = getenv("PATH");
	if (!path) return false; // something is horribly wrong...
	// we are sure we won't need a buffer any longer
	char *buf = myalloc(strlen(path) + strlen(cmd) + 3);
	if(!buf) return false; // actually useless, see comment
	// loop as long as we have stuff to examine in path
	for(; *path; ++path) {
		// start from the beginning of the buffer
		char *p = buf;
		// copy in buf the current path element
		for(; *path && *path != ':'; ++path, ++p) {
			*p = *path;
		}
		// empty path entries are treated like "."
		if (p == buf) *p++='.';
		// slash and command name
		if (p[-1] != '/') *p++ ='/';
		strcpy(p, cmd);
		// check if we can execute it
		if (access(buf, X_OK) == 0) {
			myfree(buf);
			return true;
		}
		// quit at last cycle
		if(!*path) break;
	}
	// not found
	myfree(buf);
	return false;
}
//------------------------------------------------------------------------------
static void _generator(FILE *mdfile) {
	char code_language[64];
	char *block, *itemval;
	lexical itemlex;
	bool ongoingCODE = false;
	bool mustexist = false;
	int currentBockQuotesNumber = 0;
	*code_language = '\0';
	while (NULL != (block = mdRead1Block(mdfile, ongoingCODE, &currentBockQuotesNumber)) && ! mustexist) {
		itemval = mdGetNextLexicalItem(block, ongoingCODE, &itemlex);
		myfree(block);
		generate_marginleftblockquote(currentBockQuotesNumber);
		switch (itemlex) {
		case EMPTY:
			printEmpty();
			break;
		case HEADER1:
			printHeader1(mdGetFirstChar(itemval));
			break;
		case HEADER2:
			printHeader2(mdGetFirstChar(itemval));
			break;
		case HEADER3:
			printHeader3(mdGetFirstChar(itemval));
			break;
		case HEADER4:
			printHeader4(mdGetFirstChar(itemval));
			break;
		case HEADER5:
			printHeader5(mdGetFirstChar(itemval));
			break;
		case HEADER6:
			printHeader6(mdGetFirstChar(itemval));
			break;
		case HLINE:
			printHline(itemval);
			break;
		case ASIS1:
			_getCodeLanguage(itemval, code_language);
			ongoingCODE = !ongoingCODE;
			break;
		case ASIS2:
			printCode(itemval, CODEWITHSPACES, code_language);
			break;
		case TEXT:
			if (ongoingCODE) printCode(itemval, CODEWITHKWORD, code_language);
			else printText(itemval);
			break;
		case LIST:
			printList(itemval, NONORDERED);
			break;
		case ORDLIST:
			printList(itemval, ORDERED);
			break;
		case TABLE:
			printTable(itemval);
			break;
		case ERR:
			mustexist = true;
			break;
		default:
			break;
		}
		myfree(itemval);
		if (mustexist) break;
		previousMarkdown = itemlex;
	}
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void myexit( int cptrdu ) {
	free_configuration(&markdown_configuration);
	freeText();
	freeEscapeSequence();
	cmdline_parser_willy_free(&args_info);
	if (check) _checker();
	exit(cptrdu);
}
//------------------------------------------------------------------------------
bool existFile( char *file ) {
	struct stat locstat;
	if (file == NULL) return false;
	if (stat(file, &locstat) < 0) return false;
	return true;
}
//------------------------------------------------------------------------------
void frewind( FILE *fd ) {
	fseek(fd, 0, SEEK_SET);
}
//------------------------------------------------------------------------------
// MAIN FUNCTION
//------------------------------------------------------------------------------
int main( int argc, char **argv ) {
	char tmpfile[TMPSIZE];
	FILE *mdfile;
	//---- Parameter checking and setting --------------------------------------
	if (cmdline_parser_willy(argc, argv, &args_info) != 0)
		myexit(EXIT_FAILURE);
	//---- Option '--check'
	check = args_info.check_given;
	//---- Configuration initialization ----------------------------------------
	display_configuration = init_configuration(&markdown_configuration);
	//---- Configuration file --------------------------------------------------
	const char *homedir;
	if ((homedir = getenv("HOME")) == NULL) {
		homedir = getpwuid(getuid())->pw_dir;
	}
	char configfile[PATH_MAX];
	snprintf(configfile, sizeof(configfile), "%s/%s", homedir, CONFIGURATION_FILE);
	if (existFile(configfile)) {
		if (! get_configuration(configfile, &display_configuration, &markdown_configuration)) {
			error("Cannot load configuration file %s.", configfile);
			goto err;
		}
	}
	//---- Option 'config' -----------------------------------------------------
	if (args_info.config_given) {
		if (existFile(args_info.config_arg)) {
			if (! get_configuration(args_info.config_arg, &display_configuration, &markdown_configuration)) {
				error("Cannot load configuration file %s.", args_info.config_arg);
				goto err;
			}
		} else {
			error("Configuration file %s does not exist", args_info.config_arg);
			goto err;
		}
	}
	//---- Option 'justification' ----------------------------------------------
	if (args_info.justify_given)
		text_alignment = JUSTIFY;
	//---- Option 'show' or 'multishow' ------------------------------------
	if (args_info.show_given || args_info.multishow_given)
		feh_openFilelist();
	//---- Option 'report' -------------------------------------------------
	if (args_info.report_given || args_info.text_given) {
		if (args_info.report_given) args_info.text_given = 0;
	}
	//---- Option 'no-syntax-coloring' -----------------------------------------
	if (CHECKINGBIT(display_configuration, CONFIGURATION_SYNTAXHIGHLIGHTING)) {
		if (args_info.no_syntax_coloring_given)
			syntax_coloring = false;
		else {
			syntax_coloring = _can_run_command(SYNTAXCOLORING);
			if (syntax_coloring) {
				CLEARBIT(display_configuration, CONFIGURATION_CODE_FOREGROUND);
				SETBIT(display_configuration, CONFIGURATION_CODE_DEFAULT);
			}
		}
	} else
		syntax_coloring = false;
	//----- Set ANSI Escape sequences according to configuration ---------------
	allocText();
	setEscapeSequence();
	//---- Processing files ----------------------------------------------------
	int n;
	bool images_still_not_displayed = true;
	bool report_still_not_displayed = true;
	do {
		n = 0;
		//---- Windows width ---------------------------------------------------
		if (! _getTerminalSize(&nbCOLS)) {
			error("%s", "Cannot get the terminal size.");
			goto err;
		}
		//---- Option 'columns' ------------------------------------------------
		if (args_info.columns_given)
			nbCOLS = (size_t)args_info.columns_arg;
		//---- Option 'output' or 'more'----------------------------------------
		if (args_info.output_given  || args_info.more_given || args_info.nodisplay_given) {
			if (-1 == (ex_stdout = dup(fileno(stdout)))) {
				error("%s", "Cannot create a new file stream.");
				goto err;
			}
			(void) close(fileno(stdout));
		}
		//---- Option consistency tuning ---------------------------------------
		if (args_info.nodisplay_given) {
			args_info.output_given = 0;
			args_info.more_given = 0;
			if (-1 == (outfd = open("/dev/null", O_WRONLY))) {
				error("%s", "Cannot open /dev/null.");
				goto err;
			}
			temporaryout = false;
		}
		//---- Option 'output' -------------------------------------------------
		if (args_info.output_given) {
			if (-1 == (outfd = creat(args_info.output_arg, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH))) {
				error("%s", "Cannot create output file.");
				goto err;
			}
			temporaryout = false;
			strcpy(tmpfile, args_info.output_arg);
		}
		//---- Option 'more' ---------------------------------------------------
		if (args_info.more_given) {
			if (! args_info.output_given) {
				if (-1 == (outfd = file_opentemporaryfile(tmpfile, GENERAL)))
					goto err;
				temporaryout = true;
			}
			nbCOLS -= 8;
			if (args_info.columns_given)
				warning("Number of columns is forced to %ld.", nbCOLS);
			strcpy(banner, "Mark-Down file(s): ");
		}
		if (args_info.report_given || args_info.text_given) {
			report_init();
		}
		//---- Go on -----------------------------------------------------------
		for (unsigned int ifile = 0 ; ifile < args_info.inputs_num ; ++ifile) {
			// Is the file parameter existing?
			if (! existFile(args_info.inputs[ifile])) {
				error("File '%s' does not exist.", args_info.inputs[ifile]);
				goto err;
			};
			// Record path of the MD file (for later, e.g. access to referenced images)
			strcpy(pathofmdfile, args_info.inputs[ifile]);
			strcpy(pathofmdfile, dirname(pathofmdfile));
			// File opening
			if (NULL == (mdfile = fopen(args_info.inputs[ifile], "r"))) {
				error("Cannot open '%s' file.", args_info.inputs[ifile]);
				goto err;
			};
			// If 'more' option, then complete the banner (tp line)
			if (args_info.more_given) {
				if (sizeof(banner) > strlen(banner) + strlen(args_info.inputs[ifile] + 1)) {
					strcat(banner, " ");
					strcat(banner, args_info.inputs[ifile]);
				}
			}
			// Text generation
			_generator(mdfile);
			if (args_info.report_given || args_info.text_given) {
				frewind(mdfile);
				report_scanner(mdfile, args_info.inputs[ifile]);
			}
			fclose(mdfile);
			fflush(stdout);
		}
		//---- Close output if file ----------------------------------------------
		if (outfd != -1) {
			(void) close(outfd);
			int unused = dup(ex_stdout);
		}
		//---- Show images if requested ------------------------------------------
		if ((args_info.show_given || args_info.multishow_given) && images_still_not_displayed) {
			feh_showImages(args_info.multishow_given, args_info.more_given);
			images_still_not_displayed = false;
		}
		//---- Display report if requested --------------------------------------
		if ((args_info.report_given || args_info.text_given) && report_still_not_displayed) {
			if (args_info.report_given)
				report_html_generation(args_info.report_arg);
			else
				report_text_generation(args_info.report_arg);
			report_still_not_displayed = false;
		}
		//---- Display if requested ----------------------------------------------
		if (args_info.more_given) {
			struct sPager control;
			control.prefixerror = (char *) "WILLY:";
			//control.resizingtype = STOP_ON_RESIZING;
			control.resizingtype = REDRAW_ON_RESIZING;
			control.header = (char *) "WILLY, a markdown text-formatting program";
			control.endbanner = (char *) "";
			control.viewtype = SYNCHRONOUS;
			control.filename =tmpfile;
			n = pager_view(control);
			if (temporaryout) file_unlinktemporaryfile(tmpfile);
		}
		//---- Kill viewer and remove temporary file if option 'show' or 'multishow'
		if (args_info.show_given || args_info.multishow_given)
			feh_killShow(args_info.more_given);
	} while (n == -2);	// Again if window has been resized....
	return_value = EXIT_SUCCESS;
end:
	myexit(return_value);
err:
	if (outfd != -1) (void) close(outfd);
	if (ex_stdout != -1) ex_stdout = dup(ex_stdout);
	return_value = EXIT_FAILURE;
	goto end;
}
//------------------------------------------------------------------------------
