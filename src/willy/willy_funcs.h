//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: willy
// Linux Markdown text-formatting program producing output suitable for simple
// fixed-width terminal windows.
//------------------------------------------------------------------------------
#ifndef WILLY_FILE_H
#define WILLY_FILE_H
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define TMPSIZE						64
//------------------------------------------------------------------------------
// TYPEDEFS
//------------------------------------------------------------------------------
typedef enum { GENERAL, FILELIST, SYNTAX, LOG } tmptype;
//------------------------------------------------------------------------------
// FUNCTIONS
//------------------------------------------------------------------------------
extern char 	*file_gettemporaryfilename( char *, tmptype );
extern int		file_opentemporaryfile( char *, tmptype );
extern void		file_unlinktemporaryfile( char * );

extern void		myfree( void * );
extern char 	*mystrdup( const char * );
extern void 	*mycalloc( size_t, size_t );
extern char 	*myalloc( size_t );
//------------------------------------------------------------------------------
#endif	// WILLY_FILE_H
