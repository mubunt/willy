//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: willy
// Linux Markdown text-formatting program producing output suitable for simple
// fixed-width terminal windows.
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <stdbool.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "willy_common.h"
#include "willy_configuration.h"
#include "willy_funcs.h"
#include "ini.h"
//------------------------------------------------------------------------------
// MACRO DEFINITIONS
//------------------------------------------------------------------------------
#define MATCH(s, n)			strcmp(section, s) == 0 && strcmp(name, n) == 0
//------------------------------------------------------------------------------
// EXTERNAL VARIABLES
//------------------------------------------------------------------------------
extern configuration 		config;					// User-definied configuration
extern int 					display_configuration;	// Configuration as defined in configuration file
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static char *lower( char  *s ) {
	for (char  *p = s; *p; ++p) *p = (char)tolower(*p);
	return s;
}
//------------------------------------------------------------------------------
static int handler(void *user, const char *section, const char *name, const char *value) {
	configuration *pconfig = (configuration *)user;

	if (MATCH("general", "syntax-highlighting")) {
		myfree(pconfig->syntaxhighlighting);
		pconfig->syntaxhighlighting = lower(mystrdup(value));

	} else if (MATCH("header1", "foregroundcolor")) {
		myfree(pconfig->h1_foregroundcolor);
		pconfig->h1_foregroundcolor = lower(mystrdup(value));
	} else if (MATCH("header1", "attribut")) {
		myfree(pconfig->h1_attribut);
		pconfig->h1_attribut = lower(mystrdup(value));
	} else if (MATCH("header2", "foregroundcolor")) {
		myfree(pconfig->h2_foregroundcolor);
		pconfig->h2_foregroundcolor = lower(mystrdup(value));
	} else if (MATCH("header2", "attribut")) {
		myfree(pconfig->h2_attribut);
		pconfig->h2_attribut = lower(mystrdup(value));
	} else if (MATCH("header3", "foregroundcolor")) {
		myfree(pconfig->h3_foregroundcolor);
		pconfig->h3_foregroundcolor = lower(mystrdup(value));
	} else if (MATCH("header3", "attribut")) {
		myfree(pconfig->h3_attribut);
		pconfig->h3_attribut = lower(mystrdup(value));
	} else if (MATCH("header4", "foregroundcolor")) {
		myfree(pconfig->h4_foregroundcolor);
		pconfig->h4_foregroundcolor = lower(mystrdup(value));
	} else if (MATCH("header4", "attribut")) {
		myfree(pconfig->h4_attribut);
		pconfig->h4_attribut = lower(mystrdup(value));
	} else if (MATCH("header5", "foregroundcolor")) {
		myfree(pconfig->h5_foregroundcolor);
		pconfig->h5_foregroundcolor = lower(mystrdup(value));
	} else if (MATCH("header5", "attribut")) {
		myfree(pconfig->h5_attribut);
		pconfig->h5_attribut = lower(mystrdup(value));
	} else if (MATCH("header6", "foregroundcolor")) {
		myfree(pconfig->h6_foregroundcolor);
		pconfig->h6_foregroundcolor = lower(mystrdup(value));
	} else if (MATCH("header6", "attribut")) {
		myfree(pconfig->h6_attribut);
		pconfig->h6_attribut = lower(mystrdup(value));

	} else if (MATCH("code", "backgroundcolor")) {
		myfree(pconfig->code_backgroundcolor);
		pconfig->code_backgroundcolor = lower(mystrdup(value));
	} else if (MATCH("code", "foregroundcolor")) {
		myfree(pconfig->code_foregroundcolor);
		pconfig->code_foregroundcolor = lower(mystrdup(value));
	} else if (MATCH("code", "attribut")) {
		myfree(pconfig->code_attribut);
		pconfig->code_attribut = lower(mystrdup(value));

	} else if (MATCH("link", "foregroundcolor")) {
		myfree(pconfig->link_foregroundcolor);
		pconfig->link_foregroundcolor = lower(mystrdup(value));
	} else if (MATCH("link", "attribut")) {
		myfree(pconfig->link_attribut);
		pconfig->link_attribut = lower(mystrdup(value));

	} else if (MATCH("image", "foregroundcolor")) {
		myfree(pconfig->image_foregroundcolor);
		pconfig->image_foregroundcolor = lower(mystrdup(value));
	} else if (MATCH("image", "attribut")) {
		myfree(pconfig->image_attribut);
		pconfig->image_attribut = lower(mystrdup(value));

	} else if (MATCH("table", "class")) {
		myfree(pconfig->table_class);
		pconfig->table_class = lower(mystrdup(value));

	} else {
		return 0;  /* unknown section/name, error */
	}
	return 1;
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
bool get_configuration( char *file, unsigned int *displayconfig, configuration *mdconfig ) {
	if (ini_parse(file, handler, mdconfig) < 0) return false;

	if (strncmp(mdconfig->syntaxhighlighting, ON, strlen(ON)) == 0)
		SETBIT(*displayconfig, CONFIGURATION_SYNTAXHIGHLIGHTING);
	else
		CLEARBIT(*displayconfig, CONFIGURATION_SYNTAXHIGHLIGHTING);

	if (strncmp(mdconfig->code_backgroundcolor, NORMAL, strlen(NORMAL)) == 0) {
		CLEARBIT(*displayconfig, CONFIGURATION_CODE_FOREGROUND);
		SETBIT(*displayconfig, CONFIGURATION_CODE_DEFAULT);
	} else {
		CLEARBIT(*displayconfig, CONFIGURATION_CODE_DEFAULT);
		SETBIT(*displayconfig, CONFIGURATION_CODE_FOREGROUND);
	}

	if (strncmp(mdconfig->table_class, HORIZON, strlen(HORIZON)) == 0) {
		CLEARBIT(*displayconfig, CONFIGURATION_TABLE_DEFAULT);
		CLEARBIT(*displayconfig, CONFIGURATION_TABLE_ZEBRA);
		CLEARBIT(*displayconfig, CONFIGURATION_TABLE_REVERSE);
		SETBIT(*displayconfig, CONFIGURATION_TABLE_HORIZON);
	} else if (strncmp(mdconfig->table_class, ZEBRA, strlen(ZEBRA)) == 0) {
		CLEARBIT(*displayconfig, CONFIGURATION_TABLE_DEFAULT);
		CLEARBIT(*displayconfig, CONFIGURATION_TABLE_HORIZON);
		CLEARBIT(*displayconfig, CONFIGURATION_TABLE_REVERSE);
		SETBIT(*displayconfig, CONFIGURATION_TABLE_ZEBRA);
	} else if (strncmp(mdconfig->table_class, REVERSE, strlen(REVERSE)) == 0) {
		CLEARBIT(*displayconfig, CONFIGURATION_TABLE_DEFAULT);
		CLEARBIT(*displayconfig, CONFIGURATION_TABLE_HORIZON);
		CLEARBIT(*displayconfig, CONFIGURATION_TABLE_ZEBRA);
		SETBIT(*displayconfig, CONFIGURATION_TABLE_REVERSE);
	} else {
		CLEARBIT(*displayconfig, CONFIGURATION_TABLE_HORIZON);
		CLEARBIT(*displayconfig, CONFIGURATION_TABLE_ZEBRA);
		CLEARBIT(*displayconfig, CONFIGURATION_TABLE_REVERSE);
		SETBIT(*displayconfig, CONFIGURATION_TABLE_DEFAULT);
	}
	return true;
}
//-----------------------------------------------------------------------------
unsigned int init_configuration( configuration *mdconfig ) {
	// [general] section
	mdconfig->syntaxhighlighting = mystrdup(ON);

	// [header1] section
	mdconfig->h1_foregroundcolor = mystrdup(YELLOW);
	mdconfig->h1_attribut = mystrdup(BOLD);
	// [header2] section
	mdconfig->h2_foregroundcolor = mystrdup(YELLOW);
	mdconfig->h2_attribut = mystrdup(BOLDUNDERLINE);
	// [header3] section
	mdconfig->h3_foregroundcolor = mystrdup(YELLOW);
	mdconfig->h3_attribut = mystrdup(BOLD);
	// [header4] section
	mdconfig->h4_foregroundcolor = mystrdup(YELLOW);
	mdconfig->h4_attribut = mystrdup(UNDERLINE);
	// [header5] section
	mdconfig->h5_foregroundcolor = mystrdup(YELLOW);
	mdconfig->h5_attribut = mystrdup(ITALIC);
	// [header6] section
	mdconfig->h6_foregroundcolor = mystrdup(YELLOW);
	mdconfig->h6_attribut = mystrdup(NORMAL);
	// [code] section
	mdconfig->code_backgroundcolor = mystrdup(NORMAL);
	mdconfig->code_foregroundcolor = mystrdup(GREEN);
	mdconfig->code_attribut = mystrdup(NORMAL);
	// [link] section
	mdconfig->link_foregroundcolor = mystrdup(BLUE);
	mdconfig->link_attribut = mystrdup(NORMAL);
	// [image] section
	mdconfig->image_foregroundcolor = mystrdup(RED);
	mdconfig->image_attribut = mystrdup(NORMAL);

	// [table] section
	mdconfig->table_class = mystrdup(DEFAULT);

	unsigned int uconfig = 0;
	SETBIT(uconfig, CONFIGURATION_CODE_DEFAULT);
	SETBIT(uconfig, CONFIGURATION_TABLE_DEFAULT);
	SETBIT(uconfig, CONFIGURATION_SYNTAXHIGHLIGHTING);
	return uconfig;
}
//------------------------------------------------------------------------------
void free_configuration( configuration *mdconfig ) {
	// [general] section
	myfree(mdconfig->syntaxhighlighting);
	// [header1] section
	myfree(mdconfig->h1_foregroundcolor);
	myfree(mdconfig->h1_attribut);
	// [header2] section
	myfree(mdconfig->h2_foregroundcolor);
	myfree(mdconfig->h2_attribut);
	// [header3] section
	myfree(mdconfig->h3_foregroundcolor);
	myfree(mdconfig->h3_attribut);
	// [header4] section
	myfree(mdconfig->h4_foregroundcolor);
	myfree(mdconfig->h4_attribut);
	// [header5] section
	myfree(mdconfig->h5_foregroundcolor);
	myfree(mdconfig->h5_attribut);
	// [header6] section
	myfree(mdconfig->h6_foregroundcolor);
	myfree(mdconfig->h6_attribut);
	// [code] section
	myfree(mdconfig->code_backgroundcolor);
	myfree(mdconfig->code_foregroundcolor);
	myfree(mdconfig->code_attribut);
	// [link] section
	myfree(mdconfig->link_foregroundcolor);
	myfree(mdconfig->link_attribut);
	// [image] section
	myfree(mdconfig->image_foregroundcolor);
	myfree(mdconfig->image_attribut);
	// [table] section
	myfree(mdconfig->table_class);
}
//------------------------------------------------------------------------------
