//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: willy
// Linux Markdown text-formatting program producing output suitable for simple
// fixed-width terminal windows.
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>
#include <stdbool.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "willy_common.h"
#include "willy_configuration.h"
#include "willy_funcs.h"
#include "willy_drawingchars.h"
#include "willy_escape.h"
//------------------------------------------------------------------------------
// EXTERNAL VARIABLES
//------------------------------------------------------------------------------
extern configuration 		markdown_configuration;	// User-definied configuration
extern ansiescapesequence	escapeSequence;			// ANSI Escape sequence for test attributs and colors
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
typedef struct {
	const char *name;
	int foreground;
	int background;
} color;
typedef struct {
	const char *name;
	int code;
} attribut;
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static color 			colors[9] 	= {
	// Color 		Foreground 	Background
	{ "black",		30,			40 },
	{ "red",		31,			41 },
	{ "green",		32,			42 },
	{ "yellow",		33,			43 },
	{ "blue",		34,			44 },
	{ "magenta",	35,			45 },
	{ "cyan",		36,			46 },
	{ "white",		37,			47 },
	{ "normal",		-1,			-1 }
};
static attribut  		attributs[6] = {
	// Attribut 		Code
	{ "bold",			1  },
	{ "underline",		4  },
	{ "italic",			3  },
	{ "normal",			0  },
	{ "boldunderline",	-1 },
	{ "",				0  }
};
const char *format1 = "\033[%dm";
const char *format2 = "\033[%d;%dm";
const char *format3 = "\033[%d;%d;%dm";
const char *format4 = "\033[%d;%d;%d;%dm";
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static char *_setAttributes( char *attribut, int i, bool reverse) {
	char sequence[64];
	int j;
	for (j = 0; j < 5; j++) if (strcmp(attribut, attributs[j].name) == 0) break;
	if (attributs[j].code == -1)	// Bold & Underline
		if (colors[i].foreground == -1)
			sprintf(sequence, format2, attributs[0].code, attributs[1].code);
		else if (reverse)
			sprintf(sequence, format4, attributs[0].code, colors[i].background, attributs[1].code, colors[i].background);
		else
			sprintf(sequence, format4, attributs[0].code, colors[i].foreground, attributs[1].code, colors[i].foreground);
	else if (colors[i].foreground == -1)
		if (attributs[j].code == 0)	// Normal => current
			strcpy(sequence, "");
		else
			sprintf(sequence, format1, attributs[j].code);
	else if (attributs[j].code == 0)	// Normal => current
		if (reverse)
			sprintf(sequence, format1, colors[i].background);
		else
			sprintf(sequence, format1, colors[i].foreground);
	else if (reverse)
		sprintf(sequence, format2, attributs[j].code, colors[i].background);
	else
		sprintf(sequence, format2, attributs[j].code, colors[i].foreground);
	return mystrdup(sequence);
}

static char *_setAttributes2( char *attribut, int ifo, char *background) {
	char sequence[64];
	int ia, ib;
	for (ia = 0; ia < 5; ia++) if (strcmp(attribut, attributs[ia].name) == 0) break;
	if (ia == 5) ia = 0;
	for (ib = 0; ib < 9; ib++) if (strcmp(background, colors[ib].name) == 0) break;
	if (ib == 9) ib = 0;

	if (attributs[ia].code == -1)	// Bold & Underline
		if (colors[ifo].foreground == -1)
			if (colors[ib].background == -1)
				sprintf(sequence, format2, attributs[0].code, attributs[1].code);
			else
				sprintf(sequence, format3, attributs[0].code, attributs[1].code, colors[ib].background);
		else if (colors[ib].background == -1)
			sprintf(sequence, format3, attributs[0].code, attributs[1].code, colors[ifo].foreground);
		else
			sprintf(sequence, format4, attributs[0].code, attributs[1].code, colors[ifo].foreground, colors[ib].background);
	else if (colors[ifo].foreground == -1)
		if (colors[ib].background == -1)
			sprintf(sequence, format1, attributs[ia].code);
		else
			sprintf(sequence, format2, attributs[ia].code, colors[ib].background);
	else if (colors[ib].background == -1)
		sprintf(sequence, format2, attributs[ia].code, colors[ifo].foreground);
	else
		sprintf(sequence, format3, attributs[ia].code, colors[ifo].foreground, colors[ib].background);

	return mystrdup(sequence);
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void setEscapeSequence( void ) {
	for (int i = 0; i < 9; i++) {
		if (strcmp(markdown_configuration.h1_foregroundcolor, colors[i].name) == 0)
			escapeSequence.h1 = _setAttributes(markdown_configuration.h1_attribut, i, false);
		if (strcmp(markdown_configuration.h2_foregroundcolor, colors[i].name) == 0)
			escapeSequence.h2 = _setAttributes(markdown_configuration.h2_attribut, i, false);
		if (strcmp(markdown_configuration.h3_foregroundcolor, colors[i].name) == 0)
			escapeSequence.h3 = _setAttributes(markdown_configuration.h3_attribut, i, false);
		if (strcmp(markdown_configuration.h4_foregroundcolor, colors[i].name) == 0)
			escapeSequence.h4 = _setAttributes(markdown_configuration.h4_attribut, i, false);
		if (strcmp(markdown_configuration.h5_foregroundcolor, colors[i].name) == 0)
			escapeSequence.h5 = _setAttributes(markdown_configuration.h5_attribut, i, false);
		if (strcmp(markdown_configuration.h6_foregroundcolor, colors[i].name) == 0)
			escapeSequence.h6 = _setAttributes(markdown_configuration.h6_attribut, i, false);
		if (strcmp(markdown_configuration.link_foregroundcolor, colors[i].name) == 0) {
			escapeSequence.link = _setAttributes(markdown_configuration.link_attribut, i, false);
			escapeSequence.link_reverse = _setAttributes(markdown_configuration.link_attribut, i, true);
		}
		if (strcmp(markdown_configuration.image_foregroundcolor, colors[i].name) == 0) {
			escapeSequence.image = _setAttributes(markdown_configuration.image_attribut, i, false);
			escapeSequence.image_reverse = _setAttributes(markdown_configuration.image_attribut, i, true);
		}
		if (strcmp(markdown_configuration.code_foregroundcolor, colors[i].name) == 0)
			escapeSequence.code = _setAttributes2(markdown_configuration.code_attribut, i, markdown_configuration.code_backgroundcolor);
	}
	escapeSequence.attributsoff = mystrdup("\033[0m");
	escapeSequence.strong_on = mystrdup("\033[1m");
	escapeSequence.emphasis_on = mystrdup("\033[3m");
	escapeSequence.strike_on = mystrdup("\033[9m");
	escapeSequence.reverse_on = mystrdup("\033[7m");
	escapeSequence.reversestrong_on = mystrdup("\033[1;7m");
}
//-----------------------------------------------------------------------------
void freeEscapeSequence( void ) {
	myfree(escapeSequence.h1);
	myfree(escapeSequence.h2);
	myfree(escapeSequence.h3);
	myfree(escapeSequence.h4);
	myfree(escapeSequence.h5);
	myfree(escapeSequence.h6);
	myfree(escapeSequence.code);
	myfree(escapeSequence.link);
	myfree(escapeSequence.link_reverse);
	myfree(escapeSequence.image);
	myfree(escapeSequence.image_reverse);
	myfree(escapeSequence.attributsoff);
	myfree(escapeSequence.strong_on);
	myfree(escapeSequence.emphasis_on);
	myfree(escapeSequence.strike_on);
	myfree(escapeSequence.reverse_on);
	myfree(escapeSequence.reversestrong_on);
}
//-----------------------------------------------------------------------------
