//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: willy
// Linux Markdown text-formatting program producing output suitable for simple
// fixed-width terminal windows.
//------------------------------------------------------------------------------
#ifndef WILLY_COMMON_H
#define WILLY_COMMON_H
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define error(_FMT, ...)			do { fprintf(stderr, "\033[1;31mERROR:\033[0m " _FMT "\n\n", __VA_ARGS__); } while (0)
#define warning(_FMT, ...)			do { fprintf(stderr, "\033[1;33mWARNING:\033[0m " _FMT "\n", __VA_ARGS__); } while (0)
#define debug(_FMT, ...)			do { fprintf(stdout, "\033[0;32mCHECK: " _FMT "\033[0m\n", __VA_ARGS__); } while (0)

#define max(_A, _B)					((_A) > (_B) ? (_A) : (_B))

#define LINE_MAX					4096
//--- Syntax Coloring -----------------------------------------------------------
#define SYNTAXCOLORING				"source-highlight"
#define SYNTAXCOLORINGCMD			"%s --input=%s --src-lang=%s --out-format=esc"
#define SYNTAXCOLORINGCMDNOLANG		"%s --input=%s --src-lang=bash --out-format=esc"
//------------------------------------------------------------------------------
// TYPEDEFS
//------------------------------------------------------------------------------
typedef enum { LEFT, CENTER, JUSTIFY } alignment;
//------------------------------------------------------------------------------
// FUNCTIONS
//------------------------------------------------------------------------------
extern bool existFile( char * );
extern void frewind( FILE * );
extern void myexit( int );
//------------------------------------------------------------------------------
#endif	// WILLY_COMMON_H
