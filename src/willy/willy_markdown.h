//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: willy
// Linux Markdown text-formatting program producing output suitable for simple
// fixed-width terminal windows.
//------------------------------------------------------------------------------
#ifndef WILLY_MARKDOWN_H
#define WILLY_MARKDOWN_H
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define MD_HLINE1			"---"
#define MD_HLINE2			"***"
#define MD_HLINE3			"___"
#define MD_CODE				"```"
#define MD_STRONG1			"**"
#define MD_STRONG2			"__"
#define MD_EMPHASIS1		"*"
#define MD_EMPHASIS2		"_"
#define MD_STRIKE			"~~"
#define MD_LIST1			"* "
#define MD_LIST2			"- "
#define MD_LIST3			"+ "
#define MD_IMAGE_START		"!["
#define MD_3DASHES			"---"

#define MD_HEADER			'#'
#define MD_BLOCKQUOTE		'>'
#define MD_LINK_START		'['
#define MD_LINK_END			']'
#define MD_IMAGE_END		']'
#define MD_URL_START		'('
#define MD_URL_END			')'
#define MD_TABLE_COLUMN		'|'
#define MD_TABLE_ALIGNMENT	':'
#define MD_ANGLE_BRACKET_R	'>'
//------------------------------------------------------------------------------
// ENUMERATIONS
//------------------------------------------------------------------------------
typedef enum { ERR, EMPTY, HEADER1, HEADER2, HEADER3, HEADER4, HEADER5, HEADER6,
               LIST, ORDLIST, TEXT, ASIS1, ASIS2, HLINE, BLCKQUOTE, TABLE
             } lexical;
//------------------------------------------------------------------------------
// FUNCTIONS
//------------------------------------------------------------------------------
extern char 	*mdGetFirstChar( char * );
extern void		mdRemoveEndingChars( char * );
extern char 	*mdRead1Block( FILE *, bool, int * );
extern char 	*mdGetNextLexicalItem( char *, bool, lexical * );
//------------------------------------------------------------------------------
#endif	// WILLY_MARKDOWN_H
