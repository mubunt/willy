//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: willy
// Linux Markdown text-formatting program producing output suitable for simple
// fixed-width terminal windows.
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "willy_common.h"
#include "willy_markdown.h"
#include "willy_funcs.h"
#include "willy_text.h"
//------------------------------------------------------------------------------
// EXTERNAL VARIABLES
//------------------------------------------------------------------------------
extern long int 			startLastListText;		// Index of the last text of a bullet of a list
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static off_t _getoffset( FILE *fd ) {
	return(ftell(fd));
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void _setoffset( FILE *fd, off_t offset) {
	fseek(fd, offset, SEEK_SET);
}
//-----------------------------------------------------------------------------
//-- Check if a line is empty, that means it contains no other characters than
//-- null, return ou space.
static bool _isAnEmptyLine( char *line ) {
	char *pt = line;
	if (*pt == MD_BLOCKQUOTE) {
		while (*pt != '\0' && (*pt == MD_BLOCKQUOTE || *pt == ' ')) ++pt;
	}
	while (*pt != '\0') {
		if (*pt != '\n' && *pt != ' ') break;
		++pt;
	}
	if (*pt == '\0') return true;
	else return false;
}
//-----------------------------------------------------------------------------
static bool _startsWithANumber( char *line ) {
	char *pt = line;
	while (*pt != '.' && *pt != '\0') ++pt;
	if (*pt == '\0') return false;
	*pt = '\0';
	int num = atoi(line);
	*pt = '.';
	if (num == 0 && line[0] != '0' && line[1] != '.') return false;
	return true;
}
//-----------------------------------------------------------------------------
static char *_countBockQuotes( char *line, int *counter ) {
	int n = *counter = 0;
	if (*line == MD_BLOCKQUOTE) {
		while (*line != '\0' && (*line == MD_BLOCKQUOTE || *line == ' ')) {
			if (*line == MD_BLOCKQUOTE) ++*counter;
			else {
				if (*line == ' ') ++n;
				if (n > 4) {
					line = line - n + 1;
					break;
				}
			}
			++line;
		}
	}
	return line;
}
//-----------------------------------------------------------------------------
static bool _check_the_rest_of_the_line( char *line ) {
	line = mdGetFirstChar(line) + strlen(MD_HLINE1);
	while (*line != '\0' && *line != '\n') {
		if (*line != ' ' && *line != '-') return false;
		++line;
	}
	return true;
}
//-----------------------------------------------------------------------------
//-- For a table, at least one character must be '|' and, at least 3 characters
//--  '-' in the first or second line.
//
//-- | Header | Header |
//-- |--------|--------|
//-- | Row    | Row    |
//--
//-- or
//--
//-- Header | Header
//-- -------|-------
//-- Row    | Row
//--
//-- or
//--
//-- -------|-------
//-- Row    | Row
//--
//-- The beginning and ending pipes being optional.
//-- Don't need to make the raw Markdown line up prettily.
static bool _lookingForTable( char *lines ) {
	char *pt = lines;
	while (*pt != '\0' && *pt != '\n') {			// At least one character '|' in the first line.
		if (*pt == MD_TABLE_COLUMN) break;
		++pt;
	}
	if (*pt != MD_TABLE_COLUMN) return false;

	while (*lines != '\0' && *lines != '\n') {		// At least 3 characters '-' in the first line.
		if (*lines == '-') {
			if (strncmp(lines, MD_3DASHES, 3) == 0) return true;
		}
		++lines;
	}
	if (*lines == '\0') return false;					// Second line.
	++lines;
	while (*lines != '\0' && *lines != '\n') {		// At least 3 characters '-' in the second line.
		if (*lines == '-') {
			if (strncmp(lines, MD_3DASHES, 3) == 0) return true;
		}
		++lines;
	}
	return false;
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
//-- Return the address of the first non-blank caracter.
char *mdGetFirstChar( char *line ) {
	while (*line == ' ') ++line;
	return line;
}
//-----------------------------------------------------------------------------
void mdRemoveEndingChars( char *line ) {
	char *pt = line + strlen(line) - 1;
	while ((*pt == ' ' || (*pt == '\n')) && pt != line) {
		*pt = '\0';
		--pt;
	}
}
//-----------------------------------------------------------------------------
//-- Read a block (or paragraph, ...)
//-- A block is ether;
//--	- a set of consecutive lines till an empty line (paragraph)
//--	- a ligne starting with a keyword as '#', '|||', etc.
//-- Result is either NULL (if error detected) or the address of the block found.
//-- Result to be freed by the caller if not NULL.
char *mdRead1Block( FILE *fd, bool codeOnGoing, int *currentBockQuotesNumber ) {
	// Step #1: record where we are in the MD file to be able to come back.
	off_t where_we_are = _getoffset(fd);
	// Step #2: read till an empty line and compute needed block size.
	char line[LINE_MAX + 1];
	size_t n = 0;
	bool firstline = true;
	bool potentiallyinatable = false;
	int counter = 0;
	while (fgets(line, sizeof(line), fd)) {
		//-- Process line to know how many blockquote characters we have
		//-- Set ptline on the first 'processable' character (after ">" and, 4 spaces at least)
		char *ptline = _countBockQuotes(line, &counter);
		if (firstline) {
			where_we_are += ptline - line;
			*currentBockQuotesNumber = counter;
			n += strlen(ptline);
			if (codeOnGoing) {
				if (strncmp(ptline, MD_CODE, 3) == 0) break;
			} else {
				// If "---" then we check if the rest of the line has no character other than space.
				if (strncmp(mdGetFirstChar(ptline), MD_HLINE1, 3) == 0 && _check_the_rest_of_the_line(ptline))
					break;
				else if ((*ptline == MD_HEADER)
				         || (strncmp(mdGetFirstChar(ptline), MD_HLINE2, 3) == 0)
				         || (strncmp(mdGetFirstChar(ptline), MD_HLINE3, 3) == 0)
				         || (strncmp(ptline, MD_CODE, 3) == 0)
				         || (_isAnEmptyLine(ptline))
				         || ((*currentBockQuotesNumber != counter) && (*currentBockQuotesNumber != 0)))
					break;
			}
			firstline = false;
			continue;
		}
		if (codeOnGoing) {
			if (strncmp(ptline, MD_CODE, 3) == 0) break;
			n += strlen(line);
			continue;
		}
		// If "---" then we check if the rest of the line has no character other than space.
		if (strncmp(mdGetFirstChar(ptline), MD_HLINE1, 3) == 0 && _check_the_rest_of_the_line(ptline))
			break;
		if ((*ptline == MD_HEADER)
		        || (strncmp(mdGetFirstChar(ptline), MD_HLINE2, 3) == 0)
		        || (strncmp(mdGetFirstChar(ptline), MD_HLINE3, 3) == 0)
		        || (strncmp(ptline, MD_CODE, 3) == 0)
		        || (strncmp(mdGetFirstChar(ptline), MD_LIST1, 2) == 0)
		        || (strncmp(mdGetFirstChar(ptline), MD_LIST2, 2) == 0)
		        || (strncmp(mdGetFirstChar(ptline), MD_LIST3, 2) == 0)
		        || ( _startsWithANumber(mdGetFirstChar(ptline)) && ! potentiallyinatable)
		        || ((*currentBockQuotesNumber != counter) && (*currentBockQuotesNumber != 0)))
			break;
		if (_lookingForTable(ptline))
			potentiallyinatable = true;
		n += strlen(line);
		if ( _isAnEmptyLine(ptline)) break;
	}
	if (n == 0 && feof(fd)) return NULL;
	// Step #3: come back in the MD file.
	_setoffset(fd, where_we_are);
	// Step #4: allocate needed memory.
	char *block = myalloc(n + 1);
	// Step #5: read the text block.
	size_t m;
	if (n != (m = fread(block, 1, n, fd))) {
		error("Error during file reading (Read %ld chars. Expected %ld).", m, n);
		return NULL;
	}
	block[n] = '\0';
	// Step #6: return address of the read block.
	return block;
}
//-----------------------------------------------------------------------------
#define GOTO(value, label)			{ *newlexitem = value; goto label; }

char *mdGetNextLexicalItem( char *item, bool codeOnGoing, lexical *newlexitem ) {
	char *itemres, *ptmp;
	char *ptitem = item;
	if (_isAnEmptyLine(item)) GOTO(EMPTY, endGetNextLexicalItem);
	if (strncmp(mdGetFirstChar(item), MD_HLINE1, 3) == 0) GOTO(HLINE, endGetNextLexicalItem);
	if (strncmp(mdGetFirstChar(item), MD_HLINE2, 3) == 0) GOTO(HLINE, endGetNextLexicalItem);
	if (strncmp(mdGetFirstChar(item), MD_HLINE3, 3) == 0) GOTO(HLINE, endGetNextLexicalItem);
	if (strncmp(item, MD_CODE, 3) == 0) GOTO(ASIS1, endGetNextLexicalItem);
	if  (codeOnGoing) GOTO(TEXT, endGetNextLexicalItem);
	if (*item == MD_HEADER) {
		if (*++ptitem != MD_HEADER) GOTO(HEADER1, endGetNextLexicalItem);
		if (*++ptitem != MD_HEADER) GOTO(HEADER2, endGetNextLexicalItem);
		if (*++ptitem != MD_HEADER) GOTO(HEADER3, endGetNextLexicalItem);
		if (*++ptitem != MD_HEADER) GOTO(HEADER4, endGetNextLexicalItem);
		if (*++ptitem != MD_HEADER) GOTO(HEADER5, endGetNextLexicalItem);
		++ptitem;
		GOTO(HEADER6, endGetNextLexicalItem);
	}
	if (strncmp(mdGetFirstChar(item), MD_LIST1, 2) == 0) GOTO(LIST, endGetNextLexicalItem);
	if (strncmp(mdGetFirstChar(item), MD_LIST2, 2) == 0) GOTO(LIST, endGetNextLexicalItem);
	if (strncmp(mdGetFirstChar(item), MD_LIST3, 2) == 0) GOTO(LIST, endGetNextLexicalItem);
	if (_startsWithANumber(mdGetFirstChar(item))) GOTO(ORDLIST, endGetNextLexicalItem);
	ptmp = mdGetFirstChar(item);
	if ((ptmp - item) >=  4) GOTO(ASIS2, endGetNextLexicalItem);	// FOR EASE OF USE!!!
	if ((ptmp - item) >= (startLastListText + 4)) GOTO(ASIS2, endGetNextLexicalItem);
	// At this stage, there are 2 possibilities: table or text.
	if (_lookingForTable(item)) GOTO(TABLE, endGetNextLexicalItem);
	GOTO(TEXT, endGetNextLexicalItem);

endGetNextLexicalItem:
	if (! codeOnGoing) mdRemoveEndingChars(ptitem);
	itemres = myalloc(strlen(ptitem) + 1);
	strcpy(itemres, ptitem);
	return itemres;
}
//-----------------------------------------------------------------------------
