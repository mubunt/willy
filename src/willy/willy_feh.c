//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: willy
// Linux Markdown text-formatting program producing output suitable for simple
// fixed-width terminal windows.
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include <errno.h>
#include <stdbool.h>
#include <linux/limits.h>
#include <sys/wait.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "willy_common.h"
#include "willy_funcs.h"
#include "willy_feh.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define FEH_NAME		"feh"
//------------------------------------------------------------------------------
// EXTERNAL VARIABLES
//------------------------------------------------------------------------------
extern char				pathofmdfile[];			// Path of the current user's MD file
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static int				filelist_fd				= -1;
static int				logfile_fd				= -1;
static pthread_t 		read_image_thread_id	= 0;
static unsigned int 	multiw 					= 0;
static int 				filedes[2];
static pid_t 			procpid					= -1;
static char				filelistname[TMPSIZE];
static char				logfilename[TMPSIZE];
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void *_feh_readImages_thread( void *arguments ) {
	char buffer[4096];
	while (1) {
		ssize_t count = read(filedes[0], buffer, sizeof(buffer));
		if (count <= 0) break;
		ssize_t unused = write(logfile_fd, buffer, (size_t)count);
	}
	// Never reached....
	pthread_exit(NULL);
}
//------------------------------------------------------------------------------
static pid_t launch( void ) {
#define ARGSIZE		11
	char *arg[ARGSIZE];
	for (int i = 0; i < ARGSIZE; i++) arg[i] = NULL;

	int idx = 0;
	arg[idx++] = mystrdup(FEH_NAME);
	arg[idx++] = mystrdup("--no-fehbg");
	arg[idx++] = mystrdup("--no-menus");
	arg[idx++] = mystrdup("--title");
	arg[idx++] = mystrdup("willy/feh: %f (%u/%l)");
	if (multiw)
		arg[idx++] = mystrdup("--multiwindow");
	arg[idx++] = mystrdup("--filelist");
	arg[idx] = mystrdup(filelistname);

	if (pipe(filedes) == -1) {
		error("Cannot create pipes to communication with %s.", FEH_NAME);
		pthread_exit(NULL);;
	}

	pid_t pid = fork();
	switch (pid) {
	case 0 : // Child
		while ((dup2(filedes[1], STDERR_FILENO) == -1) && (errno == EINTR)) {}
		close(filedes[1]);
		close(filedes[0]);
		if (execvp(*arg, arg) < 0) {	// execute the command
			error("Cannot launch '%s' tool to show images!!!", FEH_NAME);
		}
		break;
	case -1: // Error
		error("Forking child process %s failed!", FEH_NAME);
		break;
	default: // Father
		break;
	}
	close(filedes[1]);
	pthread_create(&read_image_thread_id, NULL, _feh_readImages_thread, (void *)"");

	sleep(2);		// 2 seconds, just to give time to sub-process to work on images....

	for (int i = 0; i < ARGSIZE; i++) if (arg[i] != NULL) myfree(arg[i]);
	return pid;
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void feh_openFilelist( void ) {
	if (-1 == (filelist_fd = file_opentemporaryfile(filelistname, FILELIST)))
		myexit(EXIT_FAILURE);
}
//------------------------------------------------------------------------------
void feh_addFileToFilelist( char *imagepath ) {
	if (filelist_fd == -1) return;
	ssize_t unused;
	if (strncmp(imagepath, "http:", 5) != 0
	        && strncmp(imagepath, "https:", 6) != 0
	        && strncmp(imagepath, "file:", 5) != 0
	        && *imagepath != '/') {
		char tmp1[PATH_MAX], tmp2[PATH_MAX] ;
		sprintf(tmp1, "%s/%s", pathofmdfile, imagepath);
		char *res = realpath(tmp1, tmp2);
		unused = write(filelist_fd, tmp2, strlen(tmp2));
	} else
		unused = write(filelist_fd, imagepath, strlen(imagepath));
	unused = write(filelist_fd, "\n", 1);
}
//------------------------------------------------------------------------------
void feh_showImages( unsigned int multiwindows, unsigned int tobethreaded ) {
	if (filelist_fd == -1) return;
	close(filelist_fd);
	filelist_fd = -1;
	multiw = multiwindows;
	if (-1 == (logfile_fd = file_opentemporaryfile(logfilename, LOG)))
		myexit(EXIT_FAILURE);
	procpid = launch();
}
//------------------------------------------------------------------------------
#define CODE_TOPLEFTCORNER			"╭"
#define CODE_TOPRIGHTCORNER			"╮"
#define CODE_BOTTOMLEFTCORNER		"╰"
#define CODE_BOTTOMRIGHTCORNER		"╯"
#define CODE_HORIZONTAL				"─"
#define CODE_VERTICAL				"│"

void feh_killShow( unsigned int threaded ) {
	if (threaded)
		kill(procpid, SIGTERM);
	else
		wait(0);
	pthread_cancel(read_image_thread_id);
	close(filedes[0]);
	close(logfile_fd);
	logfile_fd = -1;

	char buffer[1024];
	sprintf(buffer, "Report on Images Display ('%s' tool)", FEH_NAME);

	fprintf(stdout, "\n");
	fprintf(stdout, "  %s", CODE_TOPLEFTCORNER);
	for (size_t i = 0; i <= strlen(buffer) + 1; i++) fprintf(stdout, "%s", CODE_HORIZONTAL);
	fprintf(stdout, "%s\n", CODE_TOPRIGHTCORNER);
	fprintf(stdout, "  %s %s %s\n", CODE_VERTICAL, buffer, CODE_VERTICAL);
	fprintf(stdout, "  %s", CODE_BOTTOMLEFTCORNER);
	for (size_t i = 0; i <= strlen(buffer) + 1; i++) fprintf(stdout, "%s", CODE_HORIZONTAL);
	fprintf(stdout, "%s\n", CODE_BOTTOMRIGHTCORNER);
	fprintf(stdout, "\n");

	FILE *file;
	bool atleastoneline = false;
	file = fopen(logfilename, "r");
	if (file) {
		while (fgets(buffer, sizeof(buffer), file) != NULL) {
			fprintf(stdout, "%s", buffer);
			atleastoneline = true;
		}
		fclose(file);
	}
	if (! atleastoneline)
		fprintf(stdout, "Nothing to report.\n");
	fprintf(stdout, "\n");

	file_unlinktemporaryfile(logfilename);
	file_unlinktemporaryfile(filelistname);
}
//-----------------------------------------------------------------------------