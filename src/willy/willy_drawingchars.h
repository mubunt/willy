//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: willy
// Linux Markdown text-formatting program producing output suitable for simple
// fixed-width terminal windows.
//------------------------------------------------------------------------------
#ifndef WILLY_DRAW_H
#define WILLY_DRAW_H
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define LINK_SEPARATOR							'!'
#define NO_COLOR								""

#define TXT_URL_START							'['
#define TXT_URL_END								']'
#define TXT_SEPARATOR							' '

#define	TXT_LINK_START							"!%"
#define	TXT_LINK_END							"%!"
#define	TXT_IMAGE_START							"!["
#define	TXT_IMAGE_END							"]!"

#define HEADER_TOPLEFTCORNER					"┌"
#define HEADER_TOPRIGHTCORNER					"┐"
#define HEADER_BOTTOMLEFTCORNER					"└"
#define HEADER_BOTTOMRIGHTCORNER				"┘"
#define HEADER_HORIZONTAL						"─"
#define HEADER_VERTICAL_LEFT					"│ "
#define HEADER_VERTICAL_RIGHT					" │"

#define CODE_TOPLEFTCORNER						"╭"
#define CODE_TOPRIGHTCORNER						"╮"
#define CODE_BOTTOMLEFTCORNER					"╰"
#define CODE_BOTTOMRIGHTCORNER					"╯"
#define CODE_HORIZONTAL							"─"
#define CODE_VERTICAL_LEFT						"│  "
#define LENGTH_CODE_VERTICAL_LEFT				3				// visual" length of CODE_VERTICAL_LEFT
#define CODE_VERTICAL_RIGHT						"  │"
#define LENGTH_CODE_VERTICAL_RIGHT				3				// "visual" length of CODE_VERTICAL_RIGHT
#define CODEBACKGROUND_MARGIN					"   "			// Same length as CODE_VERTICAL_LEFT and CODE_VERTICAL_RIGHT

#define HORIZONTAL_LINE							"─"

#define LEFTMARGIN								" "
#define LENGTH_LM								1				// "visual" length of LEFTMARGIN
#define RIGHTMARGIN								" "
#define LENGTH_RM								1				// "visual" length of RIGHTMARGIN
#define LEFTMARGINBLOCKQUOTE					"▒ "
#define LENGTH_LMBQ								2				// "visual" length of LEFTMARGINBLOCKQUOTE
#define LEFTMARGINHEADER2						""
#define LENGTH_LMH2								0				// "visual" length of LEFTMARGINHEADER2
#define LEFTMARGINHEADER3						"  "
#define LENGTH_LMH3								2				// "visual" length of LEFTMARGINHEADER3
#define LEFTMARGINHEADER4						"    "
#define LENGTH_LMH4								4				// "visual" length of LEFTMARGINHEADER4
#define LEFTMARGINHEADER5						"      "
#define LENGTH_LMH5								6				// "visual" length of LEFTMARGINHEADER5
#define LEFTMARGINHEADER6						"        "
#define LENGTH_LMH6								8				// "visual" length of LEFTMARGINHEADER6

#define LEFTCODE_MARGIN							"  "

#define DOT1									"  ⬥  "
#define DOT2									"  •  "
#define DOT3									"  ◦  "
#define DOT4									"  -  "
#define LEFTMARGINLIST							"     "

#define TABLE_TOP_TABCOL_LEFT_CORNER			"╭"
#define TABLE_TOP_TABCOL_RIGHT_CORNER			"╮"
#define TABLE_BOTTOM_TABCOL_LEFT_CORNER			"╰"
#define TABLE_BOTTOM_TABCOL_RIGHT_CORNER		"╯"

#define TABLE_TOP								"─"
#define TABLE_TOP_MIDDLE						"┬"

#define TABLE_BOTTOM							"─"
#define TABLE_BOTTOM_MIDDLE						"┴"

#define TABLE_TABCOL_LEFT_SIDE					"│"
#define TABLE_MIDDLE_SIDE						"│"
#define TABLE_TABCOL_RIGHT_SIDE					"│"

#define TABLE_TABCOL_LEFT_SEPARATOR				"├"
#define TABLE_TABCOL_RIGHT_SEPARATOR			"┤"
#define TABLE_MIDDLE_SEPARATOR					"┼"
#define TABLE_SEPARATOR 						"─"

#define TABLE_TABCOL_LEFT_HEADER_SEPARATOR		"┝"
#define TABLE_TABCOL_RIGHT_HEADER_SEPARATOR 	"┥"
#define TABLE_MIDDLE_HEADER_SEPARATOR			"┿"
#define TABLE_HEADER_SEPARATOR 					"━"
#define TABLE_MIDDLE_HEADER_SEPARATOR2			"┷"
#define TABLE_TABCOL_RIGHT_HEADER_SEPARATOR2	"┙"
//------------------------------------------------------------------------------
#endif	// WILLY_DRAW_H
