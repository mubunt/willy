//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: willy
// Linux Markdown text-formatting program producing output suitable for simple
// fixed-width terminal windows.
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "willy_common.h"
#include "willy_text.h"
#include "willy_table.h"
#include "willy_markdown.h"
#include "willy_funcs.h"
#include "willy_configuration.h"
#include "willy_escape.h"
#include "willy_drawingchars.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define INTERMEDIATE_SIZE				1024
//------------------------------------------------------------------------------
// TYPEDEFS
//------------------------------------------------------------------------------
typedef enum { TABCOL_LEFT, TABCOL_CENTER, TABCOL_RIGHT } colalignment;

typedef struct {
	size_t		numberoflines;
	size_t		numberofcolumns;
	int			headerseparator;
} s_table;

typedef struct {
	char 		*line;
	size_t		numberofcolumns;
} s_line;

typedef struct {
	size_t			colsize;
	size_t			colsizewithattributs;
	colalignment	colalign;
	char 			**column;
	char 			**current;
	bool			strong_attribut;	// 'strong_attribut' attribut on going between 2 lines
	bool			emphasis_attribut;	// 'emphasis_attribut' attribut on going between 2 lines
	bool			strike_attribut;	// 'strike_attribut' attribut on going between 2 lines
	bool			link_attribut;		// 'link_attribut' attribut on going between 2 lines
	bool			image_attribut;		// 'image_attribut' attribut on going between 2 lines
} s_column;
//------------------------------------------------------------------------------
// EXTERNAL VARIABLES
//------------------------------------------------------------------------------
extern size_t				nbCOLS ;
extern ansiescapesequence	escapeSequence;
extern char 				*marginleftblockquote;
extern  unsigned int 		display_configuration;	// Configuration as defined in configuration file
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static s_table		table;
static s_line 		**table_lines;
static s_column 	**table_columns;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static colalignment _get_column_alignment( char *value ) {
	colalignment algmnt;
	if (value[0] == MD_TABLE_ALIGNMENT && value[strlen(value) - 1] == MD_TABLE_ALIGNMENT) algmnt = TABCOL_CENTER;
	else if (value[0] == MD_TABLE_ALIGNMENT) algmnt = TABCOL_LEFT;
	else if (value[strlen(value) - 1] == MD_TABLE_ALIGNMENT) algmnt = TABCOL_RIGHT;
	else algmnt = TABCOL_LEFT;
	return algmnt;
}
//------------------------------------------------------------------------------
static void _get_table_lines( char *str, size_t maxwidth ) {
	// Initializations
	table.numberoflines = table.numberofcolumns = 0;
	table.headerseparator = -1;
	// Compute number of lines in the table.
	char *ptstr = str;
	char *ptstr1 = str;
	while (*ptstr != '\0') {
		if (*ptstr == '\n') {
			++table.numberoflines;
			*ptstr = '\0';
			if (table.headerseparator == -1) {
				if (NULL != strstr(ptstr1, MD_3DASHES)) table.headerseparator = (int) table.numberoflines - 1;
			}
			*ptstr = '\n';
			++ptstr1;
		}
		++ptstr;
	}
	// Allocation of line structure address table.
	table_lines = mycalloc(sizeof(s_line *), table.numberoflines);
	// Allocation and filling of each line stucture.
	ptstr = str;
	size_t idx  = 0;
	size_t nbcol = 0;
	bool startline = true;
	char *pstart = str;
	while (*ptstr != '\0') {
		if (startline && *ptstr == ' ') {
			++ptstr;
			pstart = ptstr;
			continue;
		}
		startline = false;
		if (*ptstr == '|') {
			if (ptstr == pstart) ++nbcol;
			else if (*(ptstr - 1) != '\\') ++nbcol;
		} else {
			if (*ptstr == '\n') {
				*ptstr = '\0';
				table_lines[idx] = mycalloc(sizeof(s_line), 1);
				table_lines[idx]->line = pstart;
				// Looking for last 'non-space' char
				char *pend = ptstr - 1;
				while (*pend == ' ') --pend;
				if (*pstart == '|' && *pend == '|') --nbcol;
				else  if (*pstart != '|' && *pend != '|' && strlen(pstart) != 0) ++nbcol;
				table_lines[idx]->numberofcolumns = nbcol;
				table.numberofcolumns = max(table.numberofcolumns, nbcol);
				nbcol = 0;
				++idx;
				startline = true;
				pstart = ptstr + 1;
			}
		}
		++ptstr;
	}
	// Allocation of column structure address table.
	table_columns = mycalloc(sizeof(s_column *), table.numberofcolumns);
	// Allocation of each column stucture.
	size_t idcol;
	for (idcol = 0; idcol < table.numberofcolumns; idcol++) {
		table_columns[idcol] = mycalloc(sizeof(s_column), 1);
		table_columns[idcol]->colsize = 0;
		table_columns[idcol]->colsizewithattributs = 0;
		table_columns[idcol]->colalign = TABCOL_LEFT;
		table_columns[idcol]->column = NULL;
		table_columns[idcol]->current = NULL;
		table_columns[idcol]->strong_attribut = false;
		table_columns[idcol]->emphasis_attribut = false;
		table_columns[idcol]->strike_attribut = false;
		table_columns[idcol]->link_attribut = false;
		table_columns[idcol]->image_attribut = false;
	}
	// Allocation of intermediate buffer.
	char **intermediate_column;
	intermediate_column = mycalloc(sizeof(char **), table.numberoflines);
	for (idx = 0; idx < table.numberoflines; idx++) {
		intermediate_column[idx] = myalloc(INTERMEDIATE_SIZE + 1);
	}
	// Filling of each column stucture via the intermediate buffer.
	for (idcol = 0; idcol < table.numberofcolumns; idcol++) {
		for (idx = 0; idx < table.numberoflines; idx++) {
			char *ptb = table_lines[idx]->line;
			if (*ptb == '\0') {
				strcpy(intermediate_column[idx], "");
			} else {
				if (*ptb == '|') ++ptb;						// Just after first '|'' if any
				while (*ptb == ' ') ++ptb;					// Ignore heading spaces
				if (*ptb == '|' || *ptb == '\0') {			// Empty cell
					strcpy(intermediate_column[idx], "");
					table_columns[idcol]->colsizewithattributs = max(table_columns[idcol]->colsizewithattributs, 1);
					table_columns[idcol]->colsize = max(table_columns[idcol]->colsize, 1);
					table_lines[idx]->line = ptb + 1;
				} else {
					char *pte = ptb;
					while (*pte != '|' && *pte != '\0') {
						if (*pte == '\\') ++pte;
						++pte;
					}
					char *ptc = pte;
					--ptc;
					while (*ptc == ' ') --ptc;				// Looking for last character of the field
					++ptc;
					*ptc = '\0';
					strncpy(intermediate_column[idx], ptb, INTERMEDIATE_SIZE);
					table_columns[idcol]->colsizewithattributs = max(table_columns[idcol]->colsizewithattributs, strlen(ptb));
					table_columns[idcol]->colsize = max(table_columns[idcol]->colsize, count_number_of_effective_characters(ptb));
					table_lines[idx]->line = pte + 1;
				}
			}
		}
		table_columns[idcol]->column = mycalloc(sizeof(char **), table.numberoflines);
		table_columns[idcol]->current = mycalloc(sizeof(char **), table.numberoflines);
		for (idx = 0; idx < table.numberoflines; idx++) {
			table_columns[idcol]->column[idx] = myalloc(table_columns[idcol]->colsizewithattributs + 1);
			strncpy(table_columns[idcol]->column[idx], intermediate_column[idx], table_columns[idcol]->colsizewithattributs + 1);
			table_columns[idcol]->current[idx] = table_columns[idcol]->column[idx];
		}
		if (table.headerseparator != -1 && strlen(table_columns[idcol]->column[table.headerseparator]) != 0)
			table_columns[idcol]->colalign = _get_column_alignment(table_columns[idcol]->column[table.headerseparator]);
	}
	// Check and recalculate, if necessary, column widths based on the width of the page.
	size_t total = 0;
	for (idcol = 0; idcol < table.numberofcolumns; idcol++) total += table_columns[idcol]->colsize;
	// 'tota'l is the sum of column with without decorations. Need to add them.
	size_t decoration = (3 * table.numberofcolumns) + 3;
	size_t supertotal = total + decoration ;
	if (supertotal > maxwidth) {
		// Average width
		size_t average = (maxwidth - decoration) / table.numberofcolumns;
		// Recalculation of width greater than the "sliding" average
		// -- Step 1: sum all widths smaller or equal to average
		size_t total1 = 0;
		size_t n = 0;
		for (idcol = 0; idcol < table.numberofcolumns; idcol++) {
			if (table_columns[idcol]->colsize <= average)
				total1 += table_columns[idcol]->colsize;
			else
				++n;
		}
		// -- Step 2: Calculate the remaining width and the new average width.
		size_t newaverage = (maxwidth - decoration - total1) / n;
		// -- Step 3: spread the remaining width over the other columns
		for (idcol = 0; idcol < table.numberofcolumns; idcol++) {
			if (table_columns[idcol]->colsize > average)
				table_columns[idcol]->colsize = newaverage;
		}
	}
	// Free intermediate buffer.
	for (idx = 0; idx < table.numberoflines; idx++) myfree(intermediate_column[idx]);
	myfree(intermediate_column);
}
//------------------------------------------------------------------------------
#define SET_ATTRIBUT(att)		{ strcat(newline, att); dest = newline + strlen(newline); }
#define PUT_CHAR(c)				*dest = c; ++dest; *dest = 0;
#define BACKSLASH()				if (backslash) { backslash = false; PUT_CHAR(*line); ++line; continue; } \
								if (*line == '\\') { backslash = true; ++line; continue; }

static char *_process_attributs( char *line, bool reverse,
                                 bool *strong_attribut, bool *strike_attribut, bool *emphasis_attribut, bool *link_attribut, bool *image_attribut ) {
	char *newline = myalloc(10 * strlen(line));		// 10 times the size, in case of ...
	char *dest = newline;
	bool backslash = false;
	*dest = 0;

	if (*strong_attribut) SET_ATTRIBUT(escapeSequence.strong_on);
	if (*strike_attribut) SET_ATTRIBUT(escapeSequence.strike_on);
	if (*emphasis_attribut) SET_ATTRIBUT(escapeSequence.emphasis_on);
	if (*link_attribut) SET_ATTRIBUT(escapeSequence.link);
	if (*image_attribut) SET_ATTRIBUT(escapeSequence.image);

	while (*line != '\0') {
		BACKSLASH()
		if (strncmp(line, MD_STRONG1, 2) == 0 || strncmp(line, MD_STRONG2, 2) == 0) {
			if (*strong_attribut) {
				SET_ATTRIBUT(escapeSequence.attributsoff)
				if (reverse) SET_ATTRIBUT(escapeSequence.reverse_on);
				if (*strike_attribut) SET_ATTRIBUT(escapeSequence.strike_on);
				if (*emphasis_attribut) SET_ATTRIBUT(escapeSequence.emphasis_on);
			} else
				SET_ATTRIBUT(escapeSequence.strong_on);
			*strong_attribut = ! *strong_attribut;
			++line;
			++line;
			continue;
		}
		if (strncmp(line, MD_STRIKE, 2) == 0) {
			if (*strike_attribut) {
				SET_ATTRIBUT(escapeSequence.attributsoff)
				if (reverse) SET_ATTRIBUT(escapeSequence.reverse_on);
				if (*strong_attribut) SET_ATTRIBUT(escapeSequence.strong_on);
				if (*emphasis_attribut) SET_ATTRIBUT(escapeSequence.emphasis_on);
			} else
				SET_ATTRIBUT(escapeSequence.strike_on);
			*strike_attribut = ! *strike_attribut;
			++line;
			++line;
			continue;
		}
		if (strncmp(line, MD_EMPHASIS1, 1) == 0 || strncmp(line, MD_EMPHASIS2, 1) == 0) {
			if (*emphasis_attribut) {
				SET_ATTRIBUT(escapeSequence.attributsoff)
				if (reverse) SET_ATTRIBUT(escapeSequence.reverse_on);
				if (*strong_attribut) SET_ATTRIBUT(escapeSequence.strong_on);
				if (*strike_attribut) SET_ATTRIBUT(escapeSequence.strike_on);
			} else
				SET_ATTRIBUT(escapeSequence.emphasis_on);
			*emphasis_attribut = ! *emphasis_attribut;
			++line;
			continue;
		}
		if (strncmp(line, TXT_IMAGE_START, 2) == 0) {
			if (reverse) SET_ATTRIBUT(escapeSequence.image_reverse)
				else SET_ATTRIBUT(escapeSequence.image);
			*image_attribut = true;
			line += 2;
			continue;
		}
		if (strncmp(line, TXT_IMAGE_END, 2) == 0) {
			SET_ATTRIBUT(escapeSequence.attributsoff);
			if (reverse) SET_ATTRIBUT(escapeSequence.reverse_on);
			if (*strong_attribut) SET_ATTRIBUT(escapeSequence.strong_on);
			if (*strike_attribut) SET_ATTRIBUT(escapeSequence.strike_on);
			if (*emphasis_attribut) SET_ATTRIBUT(escapeSequence.emphasis_on);
			if (*link_attribut) SET_ATTRIBUT(escapeSequence.link);
			*image_attribut = false;
			line += 2;
			continue;
		}
		if (strncmp(line, TXT_LINK_START, 2) == 0) {
			if (reverse) SET_ATTRIBUT(escapeSequence.link_reverse)
				else SET_ATTRIBUT(escapeSequence.link);
			*link_attribut = true;
			line += 2;
			continue;
		}
		if (strncmp(line, TXT_LINK_END, 2) == 0) {
			SET_ATTRIBUT(escapeSequence.attributsoff);
			if (reverse) SET_ATTRIBUT(escapeSequence.reverse_on);
			if (*strong_attribut) SET_ATTRIBUT(escapeSequence.strong_on);
			if (*strike_attribut) SET_ATTRIBUT(escapeSequence.strike_on);
			if (*emphasis_attribut) SET_ATTRIBUT(escapeSequence.emphasis_on);
			if (*image_attribut) SET_ATTRIBUT(escapeSequence.image);
			*link_attribut = false;
			line += 2;
			continue;
		}
		PUT_CHAR(*line);
		++line;
	}
	// If some attributs have been set, then put off them.
	if (*strong_attribut || *strike_attribut || *emphasis_attribut || *link_attribut || *image_attribut) {
		SET_ATTRIBUT(escapeSequence.attributsoff)
		if (reverse) SET_ATTRIBUT(escapeSequence.reverse_on);
	}
	return newline;
}

static char *_process_attributs_header( char *line, bool reverse,
                                        bool *strong_attribut, bool *strike_attribut, bool *emphasis_attribut, bool *link_attribut, bool *image_attribut ) {
	char *newline;
	newline = myalloc(3 * strlen(line));		// 3 times the size, in case of ...
	char *dest = newline;
	bool backslash = false;
	*dest = 0;

	if (*strong_attribut) SET_ATTRIBUT(escapeSequence.strong_on);
	if (*strike_attribut) SET_ATTRIBUT(escapeSequence.strike_on);
	if (*emphasis_attribut) SET_ATTRIBUT(escapeSequence.emphasis_on);
	if (*link_attribut) SET_ATTRIBUT(escapeSequence.link);
	if (*image_attribut) SET_ATTRIBUT(escapeSequence.image);

	while (*line != '\0') {
		BACKSLASH()
		if (strncmp(line, MD_STRONG1, 2) == 0 || strncmp(line, MD_STRONG2, 2) == 0) {
			line += 2;
			continue;
		}
		if (strncmp(line, MD_STRIKE, 2) == 0) {
			if (*strike_attribut) {
				SET_ATTRIBUT(escapeSequence.attributsoff)
				if (reverse) SET_ATTRIBUT(escapeSequence.reversestrong_on)
					else SET_ATTRIBUT(escapeSequence.strong_on);
				if (*emphasis_attribut) SET_ATTRIBUT(escapeSequence.emphasis_on);
			} else SET_ATTRIBUT(escapeSequence.strike_on);
			*strike_attribut = ! *strike_attribut;
			line += 2;
			continue;
		}
		if (strncmp(line, MD_EMPHASIS1, 1) == 0 || strncmp(line, MD_EMPHASIS2, 1) == 0) {
			if (*emphasis_attribut) {
				SET_ATTRIBUT(escapeSequence.attributsoff)
				if (reverse) SET_ATTRIBUT(escapeSequence.reversestrong_on)
					else SET_ATTRIBUT(escapeSequence.strong_on);
				if (*strike_attribut) SET_ATTRIBUT(escapeSequence.strike_on);
			} else SET_ATTRIBUT(escapeSequence.emphasis_on);
			*emphasis_attribut = ! *emphasis_attribut;
			++line;
			continue;
		}
		if (strncmp(line, TXT_IMAGE_START, 2) == 0) {
			if (reverse) SET_ATTRIBUT(escapeSequence.image_reverse)
				else SET_ATTRIBUT(escapeSequence.image);
			*image_attribut = true;
			line += 2;
			continue;
		}
		if (strncmp(line, TXT_IMAGE_END, 2) == 0) {
			SET_ATTRIBUT(escapeSequence.attributsoff)
			SET_ATTRIBUT(escapeSequence.strong_on)
			if (*strike_attribut) SET_ATTRIBUT(escapeSequence.strike_on);
			if (*emphasis_attribut) SET_ATTRIBUT(escapeSequence.emphasis_on);
			if (*link_attribut) SET_ATTRIBUT(escapeSequence.link);
			*image_attribut = false;
			line += 2;
			continue;
		}
		if (strncmp(line, TXT_LINK_START, 2) == 0) {
			if (reverse) SET_ATTRIBUT(escapeSequence.link_reverse)
				else SET_ATTRIBUT(escapeSequence.link);
			*link_attribut = true;
			line += 2;
			continue;
		}
		if (strncmp(line, TXT_LINK_END, 2) == 0) {
			SET_ATTRIBUT(escapeSequence.attributsoff);
			if (reverse) SET_ATTRIBUT(escapeSequence.reversestrong_on)
				else SET_ATTRIBUT(escapeSequence.strong_on);
			if (*strike_attribut) SET_ATTRIBUT(escapeSequence.strike_on);
			if (*emphasis_attribut) SET_ATTRIBUT(escapeSequence.emphasis_on);
			if (*image_attribut) SET_ATTRIBUT(escapeSequence.image);
			*link_attribut = false;
			line += 2;
			continue;
		}
		PUT_CHAR(*line);
		++line;
	}
	// If some attributs have been set, then put off them.
	if (*strike_attribut || *emphasis_attribut || *link_attribut || *image_attribut) {
		SET_ATTRIBUT(escapeSequence.attributsoff);
		SET_ATTRIBUT(escapeSequence.strong_on);
	}
	return newline;
}
//------------------------------------------------------------------------------
#define X_CONFIGURATION_TABLE_HORIZON		2
#define X_CONFIGURATION_TABLE_ZEBRA			4
#define X_CONFIGURATION_TABLE_REVERSE		8

static void print_toptable( unsigned int class ) {
	switch (class) {
	case X_CONFIGURATION_TABLE_HORIZON:
		fprintf(stdout, "%s%s", LEFTMARGIN, marginleftblockquote);
		for (size_t idcol = 0; idcol < table_lines[0]->numberofcolumns; idcol++) {
			for (size_t i = 0; i < table_columns[idcol]->colsize; i++)
				fprintf(stdout, "%s", TABLE_TOP);
			if (idcol == table_lines[0]->numberofcolumns - 1)
				fprintf(stdout, "\n");
			else
				fprintf(stdout, "%s", TABLE_TOP);
		}
		break;
	case X_CONFIGURATION_TABLE_ZEBRA:
	case X_CONFIGURATION_TABLE_REVERSE:
		//fprintf(stdout, "\n");
		break;
	default:
		fprintf(stdout, "%s%s%s", LEFTMARGIN, marginleftblockquote, TABLE_TOP_TABCOL_LEFT_CORNER);
		for (size_t idcol = 0; idcol < table_lines[0]->numberofcolumns; idcol++) {
			for (size_t i = 0; i < table_columns[idcol]->colsize + 2; i++)
				fprintf(stdout, "%s", TABLE_TOP);
			if (idcol == table_lines[0]->numberofcolumns - 1)
				fprintf(stdout, "%s\n", TABLE_TOP_TABCOL_RIGHT_CORNER);
			else
				fprintf(stdout, "%s", TABLE_TOP_MIDDLE);
		}
		break;
	}
}

static void print_bottomtable( unsigned int class ) {
	switch (class) {
	case X_CONFIGURATION_TABLE_HORIZON:
		fprintf(stdout, "%s%s", LEFTMARGIN, marginleftblockquote);
		for (size_t idcol = 0; idcol < table.numberofcolumns; idcol++) {
			for (size_t i = 0; i < table_columns[idcol]->colsize; i++)
				fprintf(stdout, "%s", TABLE_BOTTOM);
			if (idcol == table.numberofcolumns - 1)
				fprintf(stdout, "\n");
			else
				fprintf(stdout, "%s", TABLE_BOTTOM);
		}
		break;
	case X_CONFIGURATION_TABLE_ZEBRA:
	case X_CONFIGURATION_TABLE_REVERSE:
		fprintf(stdout, "\n");
		break;
	default:
		fprintf(stdout, "%s%s%s", LEFTMARGIN, marginleftblockquote, TABLE_BOTTOM_TABCOL_LEFT_CORNER);
		for (size_t idcol = 0; idcol < table.numberofcolumns; idcol++) {
			for (size_t i = 0; i < table_columns[idcol]->colsize + 2; i++)
				fprintf(stdout, "%s", TABLE_BOTTOM);
			if (idcol == table.numberofcolumns - 1)
				fprintf(stdout, "%s\n", TABLE_BOTTOM_TABCOL_RIGHT_CORNER);
			else
				fprintf(stdout, "%s", TABLE_BOTTOM_MIDDLE);
		}
		break;
	}
}

static void print_header_separation( unsigned int class ) {
	switch (class) {
	case X_CONFIGURATION_TABLE_HORIZON:
		fprintf(stdout, "%s%s", LEFTMARGIN, marginleftblockquote);
		for (size_t idcol = 0; idcol < table_lines[1]->numberofcolumns; idcol++) {
			for (size_t i = 0; i < table_columns[idcol]->colsize; i++)
				fprintf(stdout, "%s", TABLE_HEADER_SEPARATOR);
			if (idcol == table_lines[1]->numberofcolumns - 1)
				fprintf(stdout, "\n");
			else
				fprintf(stdout, "%s", TABLE_HEADER_SEPARATOR);
		}
		break;
	case X_CONFIGURATION_TABLE_ZEBRA:
		break;
	case X_CONFIGURATION_TABLE_REVERSE:
		fprintf(stdout, "%s%s%s", LEFTMARGIN, marginleftblockquote,escapeSequence.reversestrong_on );
		for (size_t idcol = 0; idcol < table_lines[1]->numberofcolumns; idcol++) {
			for (size_t i = 0; i < table_columns[idcol]->colsize + 2; i++)
				fprintf(stdout, "%s", TABLE_HEADER_SEPARATOR);
			if (idcol == table_lines[1]->numberofcolumns - 1)
				fprintf(stdout, "%s\n", escapeSequence.attributsoff);
			else
				fprintf(stdout, "%s %s", escapeSequence.attributsoff, escapeSequence.reversestrong_on);
		}
		break;
	default:
		fprintf(stdout, "%s%s%s", LEFTMARGIN, marginleftblockquote, TABLE_TABCOL_LEFT_HEADER_SEPARATOR);
		for (size_t idcol = 0; idcol < table_lines[1]->numberofcolumns; idcol++) {
			for (size_t i = 0; i < table_columns[idcol]->colsize + 2; i++)
				fprintf(stdout, "%s", TABLE_HEADER_SEPARATOR);
			if (idcol == table_lines[1]->numberofcolumns - 1)
				if (idcol >= table_lines[2]->numberofcolumns)
					fprintf(stdout, "%s\n", TABLE_TABCOL_RIGHT_HEADER_SEPARATOR2);
				else
					fprintf(stdout, "%s\n", TABLE_TABCOL_RIGHT_HEADER_SEPARATOR);
			else if (idcol >= table_lines[2]->numberofcolumns)
				fprintf(stdout, "%s", TABLE_MIDDLE_HEADER_SEPARATOR2);
			else
				fprintf(stdout, "%s", TABLE_MIDDLE_HEADER_SEPARATOR);
		}
		break;
	}
}

static void print_start_headerline( unsigned int class ) {
	fprintf(stdout, "%s%s", LEFTMARGIN, marginleftblockquote);
	switch (class) {
	case X_CONFIGURATION_TABLE_HORIZON:
		break;
	case X_CONFIGURATION_TABLE_ZEBRA:
	case X_CONFIGURATION_TABLE_REVERSE:
		fprintf(stdout, "%s", escapeSequence.reversestrong_on);
		break;
	default:
		fprintf(stdout, "%s%s", escapeSequence.strong_on, TABLE_TABCOL_LEFT_SIDE);
		break;
	}
}

static void print_startline( unsigned int class, size_t idx ) {
	fprintf(stdout, "%s%s", LEFTMARGIN, marginleftblockquote);
	switch (class) {
	case X_CONFIGURATION_TABLE_HORIZON:
		break;
	case X_CONFIGURATION_TABLE_ZEBRA:
		if ((idx % 2) != 0) fprintf(stdout, "%s", escapeSequence.reverse_on);
		break;
	case X_CONFIGURATION_TABLE_REVERSE:
		fprintf(stdout, "%s", escapeSequence.reverse_on);
		break;
	default:
		fprintf(stdout, "%s", TABLE_TABCOL_LEFT_SIDE);
		break;
	}
}

#define PRINT_NEXT_HEADERLINE()		switch (class) { \
									case X_CONFIGURATION_TABLE_HORIZON: \
										break; \
									case X_CONFIGURATION_TABLE_ZEBRA: \
										if (idcol != table_lines[0]->numberofcolumns - 1) \
											fprintf(stdout, " "); \
										else \
											fprintf(stdout, "%s", escapeSequence.attributsoff); \
										break; \
									case X_CONFIGURATION_TABLE_REVERSE: \
										fprintf(stdout, "%s", escapeSequence.attributsoff); \
										if (idcol != table_lines[0]->numberofcolumns - 1) \
											fprintf(stdout, " %s", escapeSequence.reversestrong_on); \
										break; \
									default: \
										if (idcol == table_lines[0]->numberofcolumns - 1) \
											fprintf(stdout, "%s%s", TABLE_TABCOL_RIGHT_SIDE, escapeSequence.attributsoff); \
										else \
											fprintf(stdout, "%s", TABLE_MIDDLE_SIDE); \
										break; \
									}

#define PRINT_SEPARATIONLINE()		n = max(table_lines[idx]->numberofcolumns, table_lines[idx - 1]->numberofcolumns); \
									switch (class) { \
									case X_CONFIGURATION_TABLE_HORIZON: \
										fprintf(stdout, "%s%s", LEFTMARGIN, marginleftblockquote); \
										for (idcol = 0; idcol < n; idcol++) { \
											for (size_t i = 0; i < table_columns[idcol]->colsize; i++) \
												fprintf(stdout, "%s", TABLE_SEPARATOR); \
											if (idcol != n - 1) \
												fprintf(stdout, "%s", TABLE_SEPARATOR); \
										} \
										fprintf(stdout, "\n"); \
										break; \
									case X_CONFIGURATION_TABLE_ZEBRA: \
										break; \
									case X_CONFIGURATION_TABLE_REVERSE: \
										fprintf(stdout, "%s%s%s", LEFTMARGIN, marginleftblockquote, escapeSequence.reversestrong_on); \
										for (idcol = 0; idcol < n; idcol++) { \
											for (size_t i = 0; i < table_columns[idcol]->colsize + 2; i++) \
												fprintf(stdout, "%s", TABLE_HEADER_SEPARATOR); \
											if (idcol == n - 1) \
												fprintf(stdout, "%s", escapeSequence.attributsoff); \
											else \
												fprintf(stdout, "%s %s", escapeSequence.attributsoff, escapeSequence.reversestrong_on); \
										} \
										fprintf(stdout, "\n"); \
										break; \
									default: \
										fprintf(stdout, "%s%s%s", LEFTMARGIN, marginleftblockquote, TABLE_TABCOL_LEFT_SEPARATOR); \
										for (idcol = 0; idcol < n; idcol++) { \
											for (size_t i = 0; i < table_columns[idcol]->colsize + 2; i++) \
												fprintf(stdout, "%s", TABLE_SEPARATOR); \
											if (idcol == n - 1) \
												if (table_lines[idx]->numberofcolumns < table_lines[idx - 1]->numberofcolumns) \
													fprintf(stdout, "%s", TABLE_BOTTOM_TABCOL_RIGHT_CORNER); \
												else if (table_lines[idx]->numberofcolumns > table_lines[idx - 1]->numberofcolumns) \
													fprintf(stdout, "%s", TABLE_TOP_TABCOL_RIGHT_CORNER); \
												else \
													fprintf(stdout, "%s", TABLE_TABCOL_RIGHT_SEPARATOR); \
											else if (idcol < table_lines[idx]->numberofcolumns) \
												if (idcol < table_lines[idx - 1]->numberofcolumns) \
													fprintf(stdout, "%s", TABLE_MIDDLE_SEPARATOR); \
												else \
													fprintf(stdout, "%s", TABLE_TOP_MIDDLE); \
											else \
												fprintf(stdout, "%s", TABLE_BOTTOM_MIDDLE); \
										} \
										fprintf(stdout, "\n"); \
										break; \
									}

#define PRINT_NEXT_LINE()			switch (class) { \
									case X_CONFIGURATION_TABLE_HORIZON: \
										if (idcol != table_lines[idx]->numberofcolumns - 1) \
											fprintf(stdout, " "); \
										break; \
									case X_CONFIGURATION_TABLE_ZEBRA: \
										if (idcol == table_lines[idx]->numberofcolumns - 1) { \
											if ((idx % 2) != 0) fprintf(stdout, "%s", escapeSequence.attributsoff); \
										} else \
											fprintf(stdout, " "); \
										break; \
									case X_CONFIGURATION_TABLE_REVERSE: \
										fprintf(stdout, "%s", escapeSequence.attributsoff); \
										if (idcol != table_lines[idx]->numberofcolumns - 1) \
											fprintf(stdout, " %s", escapeSequence.reverse_on); \
										break; \
									default: \
										if (idcol == table_lines[idx]->numberofcolumns - 1) \
											fprintf(stdout, "%s", TABLE_TABCOL_RIGHT_SIDE); \
										else \
											fprintf(stdout, "%s", TABLE_MIDDLE_SIDE); \
										break; \
									}

#define PRINT_CENTER(_N)			if (_N == table_columns[idcol]->colsize) \
										sprintf(buftmp, "%s%s%s", left, expansedvalue, right);  \
									else { \
										n = table_columns[idcol]->colsize - _N; \
										if ((n / 2) <= 0) { \
											sprintf(buftmp, "%s%s%*s%s", left, expansedvalue, (int)(n - (n / 2)), " ", right); \
										} else { \
											sprintf(buftmp, "%s%*s%s%*s%s", left, (int) (n / 2), " ", expansedvalue, (int)(n - (n / 2)), " ", right);  \
										} \
									}
#define PRINT_RIGHT(_N)				if (_N == table_columns[idcol]->colsize) \
										sprintf(buftmp, "%s%s%s", left, expansedvalue, right); \
									else \
										sprintf(buftmp, "%s%*s%s%s", left, (int)(table_columns[idcol]->colsize - _N), " ", expansedvalue, right);
#define PRINT_LEFT(_N)				if (_N == table_columns[idcol]->colsize) \
										sprintf(buftmp, "%s%-s%s", left, expansedvalue, right); \
									else \
										sprintf(buftmp, "%s%-s%*s%s", left, expansedvalue, (int)(table_columns[idcol]->colsize - _N), " ", right);

static void _print_table( void ) {
	size_t idcol, n;
	char left[2], right[2];

	unsigned int class = (display_configuration & 0x3f) >> 2;
	switch (class) {
	case X_CONFIGURATION_TABLE_HORIZON:
		*left = *right = '\0';
		break;
	default:
		strcpy(left, " ");
		strcpy(right, " ");
		break;
	}

	char *buftmp = myalloc(nbCOLS * 3);		// 3 times the length of a line!!!!

	print_toptable(class);
	bool still_lines_to_print = false;
	if (table.headerseparator > 0) {
		// --- Headers
		bool reverse;
		if (class == X_CONFIGURATION_TABLE_ZEBRA) reverse = true;
		else reverse = (class == X_CONFIGURATION_TABLE_REVERSE);
		char *expansedcolname = NULL;
		do {
			still_lines_to_print = false;
			expansedcolname = NULL;
			print_start_headerline(class);
			for (idcol = 0; idcol < table_lines[0]->numberofcolumns; idcol++) {
				size_t nbchars = count_number_of_effective_characters(table_columns[idcol]->current[0]);
				if (nbchars == 0) {
					sprintf(buftmp, "%s%*s%s", left, (int)table_columns[idcol]->colsize, " ", right);
				} else {
					if (nbchars == table_columns[idcol]->colsize) {
						expansedcolname = _process_attributs_header(table_columns[idcol]->current[0], reverse, &table_columns[idcol]->strong_attribut, &table_columns[idcol]->strike_attribut, &table_columns[idcol]->emphasis_attribut, &table_columns[idcol]->link_attribut, &table_columns[idcol]->image_attribut);
						sprintf(buftmp, "%s%s%s", left, expansedcolname, right);
						table_columns[idcol]->current[0] = table_columns[idcol]->current[0] + strlen(table_columns[idcol]->current[0]);
					} else {
						if (nbchars < table_columns[idcol]->colsize) {
							expansedcolname = _process_attributs_header(table_columns[idcol]->current[0], reverse, &table_columns[idcol]->strong_attribut, &table_columns[idcol]->strike_attribut, &table_columns[idcol]->emphasis_attribut, &table_columns[idcol]->link_attribut, &table_columns[idcol]->image_attribut);
							size_t n = table_columns[idcol]->colsize - nbchars;
							if ((n / 2) == 0)
								sprintf(buftmp, "%s%s%*s%s", left, expansedcolname, (int) (n - (n / 2)), " ", right);
							else
								sprintf(buftmp, "%s%*s%s%*s%s", left, (int) (n / 2), " ", expansedcolname, (int) (n - (n / 2)), " ", right);
							table_columns[idcol]->current[0] = table_columns[idcol]->current[0] + strlen(table_columns[idcol]->current[0]);
						} else {
							size_t k, nbspaces;
							char *ptend = give_part_of(table_columns[idcol]->current[0], table_columns[idcol]->colsize, &k, &nbspaces);
							char c = *ptend;
							*ptend = '\0';
							expansedcolname = _process_attributs_header(table_columns[idcol]->current[0], reverse, &table_columns[idcol]->strong_attribut, &table_columns[idcol]->strike_attribut, &table_columns[idcol]->emphasis_attribut, &table_columns[idcol]->link_attribut, &table_columns[idcol]->image_attribut);
							size_t n = table_columns[idcol]->colsize - k;
							if ((n / 2) == 0)
								sprintf(buftmp, "%s%s%*s%s", left, expansedcolname, (int) (n - (n / 2)), " ", right);
							else
								sprintf(buftmp, "%s%*s%s%*s%s", left, (int) (n / 2), " ", expansedcolname, (int) (n - (n / 2)), " ", right);
							*ptend = c;
							table_columns[idcol]->current[0] = ptend;
							still_lines_to_print |= true;
						}
					}
					myfree(expansedcolname);
				}
				fprintf(stdout, "%s", buftmp);
				PRINT_NEXT_HEADERLINE();
			}
			fprintf(stdout, "\n");
		} while (still_lines_to_print);
		print_header_separation(class);
	}
	// --- Values
	for (size_t idx = (size_t) (table.headerseparator + 1); idx < table.numberoflines; idx++) {
		bool reverse = (class == X_CONFIGURATION_TABLE_REVERSE);
		if (class == X_CONFIGURATION_TABLE_ZEBRA) reverse = (idx % 2) != 0;
		// --- Separation line
		if (idx != (size_t) (table.headerseparator + 1)) {
			PRINT_SEPARATIONLINE();
		}
		// --- Line of values
		char *expansedvalue = NULL;
		do {
			still_lines_to_print = false;
			expansedvalue = NULL;
			print_startline(class, idx);
			for (idcol = 0; idcol < table_lines[idx]->numberofcolumns; idcol++) {
				size_t nbchars = count_number_of_effective_characters(table_columns[idcol]->current[idx]);
				if (nbchars == 0) {
					sprintf(buftmp, "%s%*s%s", left, (int)table_columns[idcol]->colsize, " ", right);
				} else {
					if (nbchars == table_columns[idcol]->colsize) {
						expansedvalue = _process_attributs(table_columns[idcol]->current[idx], reverse, &table_columns[idcol]->strong_attribut, &table_columns[idcol]->strike_attribut, &table_columns[idcol]->emphasis_attribut, &table_columns[idcol]->link_attribut, &table_columns[idcol]->image_attribut);
						sprintf(buftmp, "%s%s%s", left, expansedvalue, right);
						table_columns[idcol]->current[idx] = table_columns[idcol]->current[idx] + strlen(table_columns[idcol]->current[idx]);
					} else {
						if (nbchars < table_columns[idcol]->colsize) {
							expansedvalue = _process_attributs(table_columns[idcol]->current[idx], reverse, &table_columns[idcol]->strong_attribut, &table_columns[idcol]->strike_attribut, &table_columns[idcol]->emphasis_attribut, &table_columns[idcol]->link_attribut, &table_columns[idcol]->image_attribut);
							switch (table_columns[idcol]->colalign) {
							case TABCOL_CENTER:
								PRINT_CENTER(nbchars) break;
							case TABCOL_RIGHT:
								PRINT_RIGHT(nbchars) break;
							case TABCOL_LEFT:
								PRINT_LEFT(nbchars) break;
							}
							table_columns[idcol]->current[idx] = table_columns[idcol]->current[idx] + strlen(table_columns[idcol]->current[idx]);
						} else {
							size_t k, nbspaces;
							char *ptend = give_part_of(table_columns[idcol]->current[idx], table_columns[idcol]->colsize, &k, &nbspaces);
							char c = *ptend;
							*ptend = '\0';
							expansedvalue = _process_attributs(table_columns[idcol]->current[idx], reverse, &table_columns[idcol]->strong_attribut, &table_columns[idcol]->strike_attribut, &table_columns[idcol]->emphasis_attribut, &table_columns[idcol]->link_attribut, &table_columns[idcol]->image_attribut);
							switch (table_columns[idcol]->colalign) {
							case TABCOL_CENTER:
								PRINT_CENTER(k) break;
							case TABCOL_RIGHT:
								PRINT_RIGHT(k) break;
							case TABCOL_LEFT:
								PRINT_LEFT(k) break;
								break;
							}
							*ptend = c;
							if (*ptend == ' ') ++ptend;
							table_columns[idcol]->current[idx] = ptend;
							still_lines_to_print |= true;
						}
					}
					myfree(expansedvalue);
				}
				fprintf(stdout, "%s", buftmp);
				PRINT_NEXT_LINE();
			}
			fprintf(stdout, "\n");
		} while (still_lines_to_print);
	}
	print_bottomtable(class);
	myfree(buftmp);
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void printTable( char *item ) {
	size_t n = strlen(item) + 2;

	char *ostr = myalloc(strlen(item) + 2);
	strcpy(ostr, item);
	if (item[strlen(item) - 1] != '\n') strcat(ostr, "\n");
	// Replacing all blockquote characters by a space.
	replace_blockquotes_with_single_spaces(ostr);
	// Replacing all consecutive space characters by 1 space.
	replace_multi_space_with_single_space(ostr);
	// Replacing image definitions with another more visual form
	char *block1 = replace_images(ostr);
	myfree(ostr);
	if (block1 == NULL) return;
	// Replacing link definitions with another more visual form
	char *block = replace_links(block1);
	myfree(block1);
	if (block == NULL) return;
	// Get and put all lines in a table recording their characteristics.
	_get_table_lines(block, nbCOLS - (LENGTH_LM + sizeMLBQ() + LENGTH_RM));
	// Now we can print the table....
	_print_table();
	// Free resources.
	myfree(block);
	for (size_t i = 0; i < table.numberoflines; i++) myfree(table_lines[i]);
	myfree(table_lines);
	for (size_t i = 0; i < table.numberofcolumns; i++) {
		for (size_t j = 0; j < table.numberoflines; j++) myfree(table_columns[i]->column[j]);
		myfree(table_columns[i]->column);
		myfree(table_columns[i]->current);
		myfree(table_columns[i]);
	}
	myfree(table_columns);
}
//-----------------------------------------------------------------------------
