# RELEASE NOTES: *willy*, a Linux Markdown text-formatting program.
___
Functional limitations of this version are described in the *README.md* file.

- **Version 1.8.9**:
  - Bug fix: Table - shift to the right of the right column separator when the value has the same width as the column.
  - Bug fix: Table - undersized allocation of a memory!
  - Bug fix: Table - attributes (strong, emphasis, etc.) are now executed in the cell where they are defined and no longer have any impact on other cells.
  - Bug fix: Typo in option definition file.
  - Bug fix: Link - the "underline" character is no longer considered as a mark-down code in instantiated urls.
  - Improvement: Replaced user-defined "boolean" type by standard one "bool".

- **Version 1.8.8**:
  - Updated build system components.

- **Version 1.8.7**:
  - Updated build system.

- **Version 1.8.6**:
  - Removed unused files.

- **Version 1.8.5**:
  - Updated build system component(s)

- **Version 1.8.4**:
  - Reworked build system to ease global and inter-project updated.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.8.3**:
  - Standardization of the installation of executables and libraries in $BIN_DIR, $LIB_DIR anetd $INC_DIR defined in the environment.

- **Version 1.8.2**:
  - Some minor changes in .comment file(s).

- **Version 1.8.1**:
  - Changed to use libPager 1.2.x (new API).
  - Fixed compilation warnings in release mode.

- **Version 1.7.6**:
  - Fixed "clean" issue in ./src/Makefile.

- **Version 1.7.5**:
  - Fixed ./Makefile and ./src/Makefile.

- **Version 1.7.4:**
  - Added tagging of new release.

- **Version 1.7.3:**
  - Improved Makefiles: version identification, optimization options (debug, release).
  - Added version identification text file.
  - Replaced license files (COPYNG, LICENSE and RELEASENOTES) by markdown version.
  - Moved from GPL v2 to GPL v3.

- **Version 1.7.2:**
  - Typo (src/willy_table.c)

- **Version 1.7.1:**
  - Typo (src/willy_text.c)
  - Tried to fix Jekyll build failure on Github (test/file1.md and test/file2.md).

- **Version 1.7.0:**
  - Added new configuration for code block display (section *code* and variale *backgroundcolor* in *.willyrc* configuration file).
  - Added a way to choose the presentation of tables (section *table* and variable *class*  in *.willyrc* configuration file).
  - Added a way to set or unset by default the syntax highlighting (section *general* and variable *syntax-highlighting* in *.willyrc* configuration file).
  - Fixed wrong display when a  triple-backticks code block is in blockquote.
  - Simplified the writing of markdown code by allowing that a line of code can be defined by 4 spaces, even if it is in a list, ordered or not.
  - Centralized and isolated escape code sequence definition.

- **Version 1.6.0:**
  - Added syntax coloring for code section.
  - Centralized and isolated temporary file management (creation, removing).
  - Centralized and isolated memory allocation management (allocation, free).
  - Added *--check* option to verify  the correctness of memory management and temporary files. 

- **Version 1.5.2:**
  - Options *--report* and *--text*: ignore code section while searching for links (including in blockquotes).

- **Version 1.5.1:**
  - Added support of URLs and URLs in angle brackets that now are automatically got turned into links.
  - Updated HTLM report summarizing all the links accordingly.
  - Added the generation, on option,  of an text report summarizing all the links and images referenced in the MD files.

- **Version 1.5.0:**
  - Added the generation, on option,  of an HTLM report summarizing all the links and images referenced in the MD files.

- **Version 1.4.0:**
  - Added support and options to show referenced images (slideshow mode or multiple windows mode).
  - Added option *--nodisplay* (*-n*) to disable text generation output.

- **Version 1.3.2:**
  - Added support of links and images in table cell.
  - Prepared integration of syntax coloring integration (still not supported).

- **Version 1.3.1:**
  - Fixed crash when a cell is emtpy (spaces) in a table.
  - Changed the internal coding of links.

- **Version 1.3.0:**
	- Added the *--more* option to display the result by paginating like 'more' or 'less' but differently.
	- Fixed some not so good stuff (like cursor positioning instead of space display).

- **Version 1.2.1:**
	- Tab characters have been taken into account in code sections.

- **Version 1.2.0:**
  - Added support of multi-line values in tables.
  - Added *--justify* (-j) option to align both the left and right ends of each line of text.
  - Run under *Valgrind*. No error detected.

- **Version 1.1.0:**
  - Added Table support.
  - Refer to README.md file for the limitations of this version.

- **Version 1.0.0:**
  - First release.
